package com.nisai.auction.graphql.instrumentation;

import com.nisai.auction.appconfiguration.utils.ApplicationSecurityContext;
import com.nisai.auction.persistence.filter.FilterConfig;
import com.nisai.auction.user.authentication.domain.UserRole;
import graphql.ExecutionResult;
import graphql.execution.AbortExecutionException;
import graphql.execution.instrumentation.InstrumentationContext;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.execution.instrumentation.parameters.InstrumentationExecuteOperationParameters;
import graphql.language.Field;
import graphql.language.OperationDefinition;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Set;

import static graphql.language.OperationDefinition.Operation.MUTATION;

@Component
@AllArgsConstructor
public class AuthInstrumentation extends SimpleInstrumentation {

    private static final Set<String> ALLOWED_OPERATION = Set.of(
            "CreateBidPrice",
            "CreateAutoBid",
            "CreateComment",
            "CreateProductOrder",
            "CreateWatchList",
            "CreateUserRate",
            "__schema");

    private final ApplicationSecurityContext context;
    private final FilterConfig filterConfig;

    @Override
    public InstrumentationContext<ExecutionResult> beginExecuteOperation(final InstrumentationExecuteOperationParameters parameters) {
        final var executionContext = parameters.getExecutionContext();
        final var operationDefinition = executionContext.getOperationDefinition();
        if (MUTATION.equals(operationDefinition.getOperation())) {
            final var user = context.authenticatedUser();
            if (user == null) {
                throw new AbortExecutionException("Authorization required");
            } else if (user.getRole() != UserRole.ADMIN && !allowedOperations(operationDefinition)) {
                filterConfig.enableFilters();
            }
        } else {
            filterConfig.enableReadFilters();
        }
        return super.beginExecuteOperation(parameters);
    }

    private boolean allowedOperations(final OperationDefinition operationDefinition) {
        final var selection = operationDefinition.getSelectionSet().getSelections().get(0);
        if (selection instanceof Field) {
            return ALLOWED_OPERATION.contains(((Field) selection).getName());
        }
        return false;
    }

}
