package com.nisai.auction.graphql.scalars;

import graphql.language.IntValue;
import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;

import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.TimeZone;

import static graphql.scalars.util.Kit.typeName;

public class LocalDateTimeScalar extends GraphQLScalarType {

    public LocalDateTimeScalar() {
        super("LocalDateTime", "LocalDateTime", new Coercing() {
            @Override
            public Object serialize(Object input) throws CoercingSerializeException {
                if (input instanceof LocalDateTime) {
                    return ((LocalDateTime) input).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                } else if (input instanceof String) {
                    return (String) input;
                }
                throw new CoercingSerializeException("Expected a 'String' or 'LocalDateTime' but was '" + typeName(input) + "'.");
            }

            @Override
            public Object parseValue(Object input) throws CoercingParseValueException {
                return serialize(input);
            }

            @Override
            public Object parseLiteral(Object input) {
                if (input instanceof StringValue) {
                    return parseStringToLocalDateTime(((StringValue) input).getValue());
                } else if (input instanceof IntValue) {
                    BigInteger value = ((IntValue) input).getValue();
                    return parseLongToLocalDateTime(value.longValue());
                }
                return null;
            }

            private LocalDateTime parseLongToLocalDateTime(long input) {
                return LocalDateTime.ofInstant(Instant.ofEpochSecond(input), TimeZone.getDefault().toZoneId());
            }

            private LocalDateTime parseStringToLocalDateTime(String input) {
                try {
                    return LocalDateTime.parse(input);
                } catch (DateTimeParseException e) {
                    return null;
                }
            }
        });

    }
}
