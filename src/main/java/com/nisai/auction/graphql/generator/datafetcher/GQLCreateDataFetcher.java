package com.nisai.auction.graphql.generator.datafetcher;

import com.nisai.auction.graphql.generator.service.GQLMutationService;
import com.nisai.auction.persistence.service.EntityNameList;
import graphql.GraphQLException;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Component;

import javax.persistence.EntityExistsException;
import javax.persistence.PersistenceException;
import java.util.Map;
import java.util.Objects;

@Component
public class GQLCreateDataFetcher extends GQLBaseDataFetcher {

    private final GQLMutationService mutationService;

    public GQLCreateDataFetcher(final GQLMutationService mutationService,
                                final EntityNameList entityNameList) {
        super(mutationService, entityNameList);
        this.mutationService = mutationService;
    }

    @Override
    public Map get(final DataFetchingEnvironment environment) {
        final var input = resolveInput(environment);
        final var entityType = getEntityType(environment);
        final var beanWrapper = new BeanWrapperImpl(entityType.getJavaType());
        try {
            input.keySet().stream()
                    .map(entityType::getAttribute)
                    .filter(Objects::nonNull)
                    .forEach(attribute -> beanWrapper.setPropertyValue(
                            attribute.getName(),
                            resolveValue(entityType, input, attribute)));
            final var entity = mutationService.save(beanWrapper.getWrappedInstance());
            return map(entity, environment.getField().getSelectionSet().getSelections());
        } catch (EntityExistsException | IllegalArgumentException e) {
            throw new GraphQLException(e.getMessage());
        } catch (PersistenceException e) {
            throw new GraphQLException("Invalid input field");
        }
    }

}
