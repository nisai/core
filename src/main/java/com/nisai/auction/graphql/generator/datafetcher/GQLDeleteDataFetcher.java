package com.nisai.auction.graphql.generator.datafetcher;

import com.nisai.auction.graphql.generator.service.GQLMutationService;
import com.nisai.auction.persistence.service.EntityNameList;
import graphql.GraphQLException;
import graphql.schema.DataFetchingEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

import static java.lang.String.format;

@Component
@Slf4j
public class GQLDeleteDataFetcher extends GQLBaseDataFetcher {

    private final GQLMutationService mutationService;

    public GQLDeleteDataFetcher(final GQLMutationService mutationService,
                                final EntityNameList entityNameList) {
        super(mutationService, entityNameList);
        this.mutationService = mutationService;
    }

    @Override
    public Map get(final DataFetchingEnvironment environment) {
        final var entityId = environment.getArgument("id");
        final var entityType = getEntityType(environment);
        try {
            final var foundEntity = mutationService.getSingleResult(entityType.getJavaType(), entityId);
            if (foundEntity != null) {
                if (mutationService.delete(foundEntity.getClass(), entityId)) {
                    return map(entityId);
                }
            }
        } catch (Exception e) {
            log.error(format("Delete %s with identifier %s failed", entityType.getName(), entityId), e);
        }
        throw new GraphQLException(format("Delete %s with identifier %s failed", entityType.getName(), entityId));
    }
}
