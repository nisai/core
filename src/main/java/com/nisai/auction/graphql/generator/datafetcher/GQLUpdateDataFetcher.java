package com.nisai.auction.graphql.generator.datafetcher;

import com.nisai.auction.graphql.generator.service.GQLMutationService;
import com.nisai.auction.persistence.service.EntityNameList;
import graphql.GraphQLException;
import graphql.schema.DataFetchingEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;

import static java.lang.String.format;

@Component
@Slf4j
public class GQLUpdateDataFetcher extends GQLBaseDataFetcher {

    private final GQLMutationService mutationService;

    public GQLUpdateDataFetcher(final GQLMutationService mutationService,
                                final EntityNameList entityNameList) {
        super(mutationService, entityNameList);
        this.mutationService = mutationService;
    }

    @Override
    public Map get(final DataFetchingEnvironment environment) throws GraphQLException {
        final var input = resolveInput(environment);
        final var entityType = getEntityType(environment);
        final var entityId = environment.getArgument("id");
        try {
            final var beanWrapper = new BeanWrapperImpl(mutationService.getSingleResult(entityType.getJavaType(), entityId));
            input.keySet().stream()
                    .map(entityType::getAttribute)
                    .filter(Objects::nonNull)
                    .forEach(attribute -> beanWrapper.setPropertyValue(
                            attribute.getName(),
                            resolveValue(entityType, input, attribute)));
            final var entity = mutationService.update(beanWrapper.getWrappedInstance());
            return map(entity, environment.getField().getSelectionSet().getSelections());
        } catch (Exception e) {
            log.error(format("Update to %s with identifier %s failed", entityType.getName(), entityId), e);
            throw new GraphQLException(e.getMessage());
        }
    }

}
