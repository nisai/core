package com.nisai.auction.graphql.annotation;

public enum GQLIgnoreGenerateType {
    ALL,
    CREATE,
    UPDATE,
    DELETE
}
