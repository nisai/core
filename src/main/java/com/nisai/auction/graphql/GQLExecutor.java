package com.nisai.auction.graphql;

import com.introproventures.graphql.jpa.query.schema.GraphQLExecutor;
import com.nisai.auction.graphql.instrumentation.AuthInstrumentation;
import com.nisai.auction.graphql.instrumentation.MaxQueryDepthInstrumentation;
import com.nisai.auction.graphql.instrumentation.MaxQuerySizeInstrumentation;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.execution.instrumentation.ChainedInstrumentation;
import graphql.schema.GraphQLSchema;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

import static javax.transaction.Transactional.TxType.SUPPORTS;

@Component
public class GQLExecutor implements GraphQLExecutor {

    private final GraphQL graphQL;

    public GQLExecutor(final GraphQLSchema graphQLSchema,
                       final MaxQuerySizeInstrumentation sizeInstrumentation,
                       final MaxQueryDepthInstrumentation depthInstrumentation,
                       final AuthInstrumentation authInstrumentation) {
        final var instrumentation = new ChainedInstrumentation(
                List.of(
                        sizeInstrumentation,
                        depthInstrumentation,
                        authInstrumentation
                ));
        this.graphQL = GraphQL.newGraphQL(
                GraphQLSchema.newSchema(graphQLSchema).build())
                .instrumentation(instrumentation)
                .build();
    }

    @Transactional(SUPPORTS)
    public ExecutionResult execute(final String query) {
        return execute(query, Map.of());
    }

    @Transactional(SUPPORTS)
    public ExecutionResult execute(final String query, final Map<String, Object> arguments) {
        return graphQL.execute(
                ExecutionInput.newExecutionInput()
                        .query(query)
                        .variables(arguments)
                        .build());
    }

}
