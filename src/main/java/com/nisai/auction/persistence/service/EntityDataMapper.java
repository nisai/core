package com.nisai.auction.persistence.service;

import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Set;

@Service
@SuppressWarnings("unchecked")
public class EntityDataMapper {

    private static final Set<String> IGNORED_FIELDS = Set.of("id", "version", "role", "status");

    @PersistenceContext
    private EntityManager entityManager;

    public <T> T mapObject(final T sourceObject, final T targetObject, Class<T> clazz) {
        final var targetWrapper = new BeanWrapperImpl(targetObject);
        final var sourceWrapper = new BeanWrapperImpl(sourceObject);
        final var en = entityManager.getMetamodel().entity(clazz);
        en.getAttributes().forEach(attribute -> {
            var attributeName = attribute.getName();
            var attributeValue = sourceWrapper.getPropertyValue(attributeName);
            if (!IGNORED_FIELDS.contains(attributeName) && attributeValue != null) {
                targetWrapper.setPropertyValue(attributeName, attributeValue);
            }
        });
        return (T) targetWrapper.getWrappedInstance();
    }
}
