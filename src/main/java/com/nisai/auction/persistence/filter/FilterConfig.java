package com.nisai.auction.persistence.filter;

import com.nisai.auction.appconfiguration.utils.ApplicationSecurityContext;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;

@Component
@AllArgsConstructor
public class FilterConfig {

    private final EntityManager entityManager;
    private final ApplicationSecurityContext context;

    public void enableFilters() {
        final var session = entityManager.unwrap(Session.class);
        enableUserFilter(session, context.authenticatedUser().getId());
        enableAdminFilter(session);
    }

    public void enableReadFilters() {
        final var session = entityManager.unwrap(Session.class);
        final var id = context.authenticatedUser() == null ? 0L : context.authenticatedUser().getId();
        session.enableFilter("readFilter").setParameter("id", id);
    }

    private void enableUserFilter(final Session session, final Long id) {
        session.enableFilter("userFilter").setParameter("id", id);
    }

    private void enableAdminFilter(final Session session) {
        session.enableFilter("adminFilter");
    }
}
