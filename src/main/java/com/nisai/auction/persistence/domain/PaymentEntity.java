package com.nisai.auction.persistence.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@MappedSuperclass
@Getter
@Setter
public abstract class PaymentEntity extends AuditingEntity {

    @NotNull
    @Column(nullable = false, unique = true)
    private String orderId;

    private BigDecimal amount;

    private String transId;

    private String currency;

    private String feeAmount;

    @Column(nullable = true, name = "tran_date")
    private String transactionDate;
}
