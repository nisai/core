package com.nisai.auction.basic.audit.domain;

public enum ActionType {
    CREATE,
    UPDATE,
    DELETE
}
