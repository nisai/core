package com.nisai.auction.basic.audit.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.nisai.auction.persistence.domain.VersionEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class AccessLog extends VersionEntity {

    private String remoteAddress;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime accessAt;
}
