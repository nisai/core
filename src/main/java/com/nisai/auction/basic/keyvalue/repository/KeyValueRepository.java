package com.nisai.auction.basic.keyvalue.repository;

import com.nisai.auction.basic.keyvalue.domain.KeyValue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface KeyValueRepository extends JpaRepository<KeyValue, Long> {
    Optional<KeyValue> findByKey(final String key);
}
