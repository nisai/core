package com.nisai.auction.basic.keyvalue.domain;

public enum KeyValueGroup {
    HOME,
    PROPERTY,
    NEWS,
    SERVICE,
    CAREER,
    ABOUT,
    CONTACT,
    OTHER
}
