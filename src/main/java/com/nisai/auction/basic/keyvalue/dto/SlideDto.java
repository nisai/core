package com.nisai.auction.basic.keyvalue.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SlideDto {
    private String imageUrl;
    private String linkUrl;
}
