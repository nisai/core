package com.nisai.auction.basic.keyvalue.service;

import com.nisai.auction.basic.keyvalue.domain.KeyValue;
import com.nisai.auction.basic.keyvalue.dto.SlideDto;
import com.nisai.auction.basic.keyvalue.repository.KeyValueRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class SlideService {

    private static final String KEY = "home_slide";

    private final KeyValueRepository repository;

    public Map<String, Object> getSlide() {
        return repository.findByKey(KEY)
                .map(KeyValue::getJsonValue)
                .orElse(null);
    }

    public void createSlide(List<SlideDto> slideDtos) {
        repository.findByKey(KEY)
                .ifPresentOrElse(
                        keyValue -> {
                            keyValue.setJsonValue(Map.of("data", slideDtos));
                            repository.save(keyValue);
                        },
                        () -> {
                            final var keyValue = new KeyValue()
                                    .setKey(KEY)
                                    .setJsonValue(Map.of("data", slideDtos));
                            repository.save(keyValue);
                        });
    }
}
