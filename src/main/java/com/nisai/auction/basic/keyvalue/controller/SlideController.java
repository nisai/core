package com.nisai.auction.basic.keyvalue.controller;

import com.nisai.auction.basic.keyvalue.dto.SlideDto;
import com.nisai.auction.basic.keyvalue.service.SlideService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/home/slide")
@RequiredArgsConstructor
public class SlideController {

    private final SlideService slideService;

    @GetMapping
    public Map<String, Object> getSlides() {
        return slideService.getSlide();
    }

    @PostMapping
    public Map<String, Object> createSlide(@RequestBody List<SlideDto> slides) {
        slideService.createSlide(slides);
        return Map.of("success", true);
    }
}
