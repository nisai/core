package com.nisai.auction.basic.health.controller;

import com.nisai.auction.appconfiguration.utils.ApplicationSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/health")
public class HealthController {

    @Autowired
    private ApplicationSecurityContext context;

    @GetMapping
    public String sayOk() {
        return "ok";
    }

    @RequestMapping(value = "/aml", produces = "text/plain")
    public String aml() {
        return "Cleared";
    }

    @RequestMapping(value = "/inbound", produces = "text/plain")
    public String success(@RequestBody String body) {
        System.out.println(body);
        return "success";
    }

    @GetMapping("/logged")
    public String sayOkYouAreLoggedIn() {
        if (context.authenticatedUser() != null) {
            return "ok you are logged in";
        }
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "not logged in yet");
    }
}
