package com.nisai.auction.basic.file.domain;

import com.nisai.auction.persistence.domain.VersionEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class FileCategory extends VersionEntity {

    @Column(nullable = false)
    private String name;
}
