package com.nisai.auction.basic.file.repository;

import com.nisai.auction.basic.file.domain.FileCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileCategoryRepository extends JpaRepository<FileCategory, Long> {
}
