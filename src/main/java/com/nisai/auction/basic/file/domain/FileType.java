package com.nisai.auction.basic.file.domain;

public enum FileType {
    IMAGE,
    VIDEO,
    SOUND,
    OTHER,
    DOCUMENT;
}
