package com.nisai.auction.basic.file.controller;

import com.nisai.auction.basic.file.domain.FileEntity;
import com.nisai.auction.basic.file.domain.FileType;
import com.nisai.auction.basic.file.repository.FileCategoryRepository;
import com.nisai.auction.basic.file.repository.FileRepository;
import com.nisai.auction.basic.file.storage.StorageFileNotFoundException;
import com.nisai.auction.basic.file.storage.StorageService;
import com.nisai.auction.basic.file.utils.FileNameUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import static com.nisai.auction.basic.file.domain.FileType.OTHER;

@Slf4j
@RestController
@RequiredArgsConstructor
public class FileUploadController {

    private final FileRepository repository;
    private final StorageService storageService;
    private final FileCategoryRepository categoryRepository;
    private final ByteArrayOutputStream thumbOutput = new ByteArrayOutputStream();

    @GetMapping("/files")
    public List<FileEntity> getAllFiles() {
        return repository.findAll();
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public byte[] serveFile(@PathVariable String filename) throws IOException {
        final var file = storageService.loadAsResource(filename);
        return file.getInputStream().readAllBytes();
    }

    @DeleteMapping(value = "/files/{filename:.+}")
    @Transactional
    public void deleteFile(@PathVariable String filename) throws IOException {
        repository.deleteByName(filename);
        storageService.deleteByName(filename);
    }

    @DeleteMapping(value = "/image/{filename:.+}")
    @Transactional
    public void deleteImage(@PathVariable String filename) throws IOException {
        repository.deleteByName(filename);
        storageService.deleteByName(filename);
    }

    @GetMapping(value = {"/x/image/{filename:.+}", "/small/image/{filename:.+}"})
    @ResponseBody
    public byte[] serveXImage(@PathVariable String filename) throws IOException {
        final var file = storageService.loadXAsResource(filename);
        return file.getInputStream().readAllBytes();
    }

    @GetMapping(value = {"/xx/image/{filename:.+}", "/medium/image/{filename:.+}"})
    @ResponseBody
    public byte[] serveXxImage(@PathVariable String filename) throws IOException {
        final var file = storageService.loadXxAsResource(filename);
        return file.getInputStream().readAllBytes();
    }

    @GetMapping(
            value = {"/image/{filename:.+}", "/image/{filename:.+}/{width}/{height}"},
            produces = {
                    MediaType.IMAGE_JPEG_VALUE,
                    MediaType.IMAGE_GIF_VALUE,
                    MediaType.IMAGE_PNG_VALUE})
    @ResponseBody
    public byte[] serveImage(@PathVariable("filename") @NotNull final String filename,
                             @RequestParam(value = "width", required = false) final Integer qWidth,
                             @RequestParam(value = "height", required = false) final Integer qHeight,
                             @PathVariable(value = "width", required = false) final Integer pWidth,
                             @PathVariable(value = "height", required = false) final Integer pHeight) throws IOException {
        final var file = storageService.loadAsResource(filename);
        final var width = qWidth == null ? pWidth : qWidth;
        final var height = qHeight == null ? pHeight : qHeight;
        if (width == null || height == null) {
            return file.getInputStream().readAllBytes();
        }
        thumbOutput.reset();
        Thumbnails.of(file.getInputStream())
                .size(width, height)
                .useOriginalFormat()
                .toOutputStream(thumbOutput);
        return thumbOutput.toByteArray();
    }

    @PostMapping("/upload")
    @Transactional
    public FileEntity handleFileUpload(@RequestParam("file") MultipartFile file,
                                       @RequestParam(value = "category", required = false) final Long categoryId) {
        if (file == null || file.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "file is empty");
        }
        var fileName = file.getOriginalFilename();
        if (fileName == null || fileName.isBlank()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "file name not found");
        }
        if (OTHER.equals(FileNameUtil.getFileType(fileName))) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "file type does not support");
        }
        final var name = storageService.store(file);
        final var newFile = new FileEntity()
                .setName(name)
                .setExtension(FileNameUtil.getFileExtension(name))
                .setType()
                .setPath();
        if (newFile.getType() == FileType.IMAGE) {
            storageService.writeXOutput(name);
            storageService.writeXxOutput(name);
        }
        if (categoryId != null) {
            final var category = categoryRepository.findById(categoryId).orElseThrow();
            newFile.setCategory(category);
        }
        return newFile;
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

}
