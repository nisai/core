package com.nisai.auction.basic.file.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.nisai.auction.basic.file.utils.FileNameUtil;
import com.nisai.auction.graphql.annotation.GQLIgnoreGenerate;
import com.nisai.auction.persistence.domain.VersionEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@GQLIgnoreGenerate
@EntityListeners(AuditingEntityListener.class)
public class FileEntity extends VersionEntity {

    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "created_at", updatable = false)
    private LocalDateTime createdAt;

    private String path;

    @Column(unique = true)
    private String name;

    private String extension;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private FileCategory category;

    @Enumerated(EnumType.STRING)
    private FileType type;

    public FileEntity setType() {
        this.type = FileNameUtil.getFileType(name);
        return this;
    }

    public FileEntity setPath() {
        if (this.type == FileType.IMAGE) {
            this.path = "/image/" + this.name;
        } else {
            this.path = "/files/" + this.name;
        }
        return this;
    }
}
