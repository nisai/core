package com.nisai.auction.basic.config.service;

import com.nisai.auction.basic.config.domain.SystemConfig;
import com.nisai.auction.basic.config.domain.SystemConfigEnum;
import com.nisai.auction.basic.config.repository.SystemConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class SystemConfigService {

    @Autowired
    private SystemConfigRepository repository;

    public List<SystemConfig> getAll(){
        return repository.findAll();
    }

    public String getValueAsString(SystemConfigEnum configEnum) {
        return getValueAsString(configEnum.name(), configEnum.getValue());
    }

    public String getValueAsString(String key, String defaultValue) {
        return repository.getValueByCode(key).orElse(defaultValue);
    }

    public Long getValueAsLong(SystemConfigEnum systemConfigEnum) {
        return Long.valueOf(getValueAsString(systemConfigEnum));
    }

    public BigDecimal getValueAsBigDecimal(SystemConfigEnum systemConfigEnum) {
        return new BigDecimal(getValueAsString(systemConfigEnum));
    }
}
