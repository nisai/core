package com.nisai.auction.basic.config.domain;

import com.nisai.auction.persistence.domain.ExtendedEntity;
import com.nisai.auction.persistence.domain.VersionEntity;
import com.nisai.auction.persistence.utils.StringListConverter;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import java.util.List;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class PageConfiguration extends VersionEntity {

    private String name;

    private String shortDescription;

    private String image;

    private String link;
}
