package com.nisai.auction.basic.config.repository;

import com.nisai.auction.basic.config.domain.SystemConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface SystemConfigRepository extends JpaRepository<SystemConfig, Long> {

    Optional<SystemConfig> findByCode(final String code);

    @Query(value = "select value from system_config where code = :code", nativeQuery = true)
    Optional<String> getValueByCode(final String code);
}
