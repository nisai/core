package com.nisai.auction.basic.config.domain;

public enum SystemConfigEnum {
    LABEL_NEW_DURATION("6"),
    LABEL_FINISH_SOON_DURATION("6"),
    BID_14_DAY_FEE("0.2"),
    BID_30_DAY_FEE("0.4"),
    COMMISSION_FEE("2"),
    SUB_TITLE_FEE("0.1"),
    BID_WIN_NOTIFICATION_TITLE("you have won a bid"),
    BID_WIN_NOTIFICATION_DETAIL("please check"),
    BID_FINISH_NOTIFICATION_TITLE("your product bid has finish"),
    INSUFFICIENT_BALANCE_NOTIFICATION_TITLE("insufficient balance"),
    INSUFFICIENT_BALANCE_NOTIFICATION_DETAIL("please charge your balance"),
    BID_FINISH_NOTIFICATION_DETAIL("please check");

    private final String value;

    SystemConfigEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
