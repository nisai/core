package com.nisai.auction.basic.notification.domain;

public enum NotificationType {
    NEGATIVE_BALANCE,
    FEE_CHARGE,
    TOP_UP_SUCCESS,
    HIGHER_BID,
    CLOSING_IN_24H,
    BID_FINISHED,
    QUESTION_ASKED,
    QUESTION_ANSWERED,
    BID,
    BID_WIN,
    ORDER
}
