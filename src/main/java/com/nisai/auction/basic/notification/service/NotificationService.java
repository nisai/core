package com.nisai.auction.basic.notification.service;

import com.nisai.auction.basic.config.domain.SystemConfigEnum;
import com.nisai.auction.basic.config.service.SystemConfigService;
import com.nisai.auction.feature.order.domain.ProductOrder;
import com.nisai.auction.feature.product.domain.BidPrice;
import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.feature.watchlist.domain.WatchList;
import com.nisai.auction.user.account.domain.UserAccount;
import com.nisai.auction.user.authentication.domain.AppUser;
import com.nisai.auction.basic.notification.domain.AppNotification;
import com.nisai.auction.basic.notification.domain.NotificationType;
import com.nisai.auction.basic.notification.repository.NotificationRepository;
import com.nisai.auction.feature.win.domain.WinProduct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Map;

@Slf4j
@Service
@Async
@RequiredArgsConstructor
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class NotificationService {

    private final SystemConfigService configService;
    private final NotificationRepository repository;

    public void bidWin(WinProduct winProduct) {
        var product = winProduct.getProduct();
        var notification = new AppNotification()
                .setTitle(configService.getValueAsString(SystemConfigEnum.BID_WIN_NOTIFICATION_TITLE))
                .setDetail(configService.getValueAsString(SystemConfigEnum.BID_WIN_NOTIFICATION_DETAIL))
                .setAppUser(winProduct.getUser())
                .setImage(product.getBgImage())
                .setData(Map.of("id", "" + product.getId()))
                .setType(NotificationType.BID_WIN);
        repository.save(notification);
    }

    public void bidFinishReserveMet(Product product) {
        var notification = new AppNotification()
                .setTitle("Your auction is closed with someone won")
                .setDetail("Your auction is closed with someone won, please contact buyer (reserve met)")
                .setAppUser(product.getCreatedBy())
                .setImage(product.getBgImage())
                .setData(Map.of("id", "" + product.getId()))
                .setType(NotificationType.BID_FINISHED);
        repository.save(notification);
    }

    public void questionAsked(Product product) {
        var notification = new AppNotification()
                .setTitle("Someone post question on your auction")
                .setDetail("Someone post question on your auction")
                .setAppUser(product.getCreatedBy())
                .setImage(product.getBgImage())
                .setData(Map.of("id", "" + product.getId()))
                .setType(NotificationType.QUESTION_ASKED);
        repository.save(notification);
    }

    public void questionAnswered(Product product) {
        var notification = new AppNotification()
                .setTitle("Someone post question on your auction")
                .setDetail("Someone post question on your auction")
                .setAppUser(product.getCreatedBy())
                .setImage(product.getBgImage())
                .setData(Map.of("id", "" + product.getId()))
                .setType(NotificationType.QUESTION_ANSWERED);
        repository.save(notification);
    }

    public void bidFinishReserveNotMet(Product product) {
        var notification = new AppNotification()
                .setTitle("Your auction is closed with no one bit or reserve price is not met")
                .setDetail("Your auction is closed with no one bit or reserve price is not met.  Relist again?")
                .setAppUser(product.getCreatedBy())
                .setImage(product.getBgImage())
                .setData(Map.of("id", "" + product.getId()))
                .setType(NotificationType.BID_FINISHED);
        repository.save(notification);
    }

    public void favoriteFinish24h(WatchList watchList) {
        var notification = new AppNotification()
                .setTitle("Your favorite auction is closing 24hours")
                .setDetail("Your favorite auction is closing 24hours")
                .setAppUser(watchList.getCreatedBy())
                .setImage(watchList.getProduct().getBgImage())
                .setData(Map.of("id", "" + watchList.getProduct().getId()))
                .setType(NotificationType.CLOSING_IN_24H);
        repository.save(notification);
    }

    public void bidFinish24h(BidPrice bidPrice) {
        var notification = new AppNotification()
                .setTitle("Your bid auction is closing 24hours")
                .setDetail("Your bid auction is closing 24hours")
                .setAppUser(bidPrice.getCreatedBy())
                .setImage(bidPrice.getProduct().getBgImage())
                .setData(Map.of("id", "" + bidPrice.getProduct().getId()))
                .setType(NotificationType.CLOSING_IN_24H);
        repository.save(notification);
    }

    public void insufficientBalance(final AppUser user) {
        final var notification = new AppNotification()
                .setTitle(configService.getValueAsString(SystemConfigEnum.INSUFFICIENT_BALANCE_NOTIFICATION_TITLE))
                .setDetail(configService.getValueAsString(SystemConfigEnum.INSUFFICIENT_BALANCE_NOTIFICATION_DETAIL))
                .setAppUser(user)
                .setImage(user.getProfileImage())
                .setType(NotificationType.NEGATIVE_BALANCE);
        repository.save(notification);
    }

    public void productOrder(final ProductOrder order) {
        var product = order.getProduct();
        final var notification = new AppNotification()
                .setTitle("Product has been ordered")
                .setDetail(String.format("Your product %s has been ordered by %s", product.getName(), order.getCreatedBy().getDisplayName()))
                .setAppUser(product.getCreatedBy())
                .setImage(product.getBgImage())
                .setData(Map.of("id", "" + product.getId()))
                .setType(NotificationType.ORDER);
        repository.save(notification);
    }

    public void productBid(BidPrice bidPrice) {
        var product = bidPrice.getProduct();
        final var notification = new AppNotification()
                .setTitle("Product has been bid")
                .setDetail(String.format("Your product %s has been bid by %s", product.getName(), bidPrice.getCreatedBy().getDisplayName()))
                .setAppUser(product.getCreatedBy())
                .setImage(product.getBgImage())
                .setData(Map.of("id", "" + product.getId()))
                .setType(NotificationType.BID);
        repository.save(notification);
    }

    public void favBid(WatchList watchList) {
        var product = watchList.getProduct();
        final var notification = new AppNotification()
                .setTitle("Someone bid on your favorite auction")
                .setDetail("Someone bid on your favorite auction")
                .setAppUser(watchList.getCreatedBy())
                .setImage(product.getBgImage())
                .setData(Map.of("id", "" + product.getId()))
                .setType(NotificationType.BID);
        repository.save(notification);
    }

    public void favRelist(WatchList watchList) {
        var product = watchList.getProduct();
        final var notification = new AppNotification()
                .setTitle("Seller relist your favorite auction")
                .setDetail("Seller relist your favorite auction")
                .setAppUser(watchList.getCreatedBy())
                .setImage(product.getBgImage())
                .setData(Map.of("id", "" + product.getId()))
                .setType(NotificationType.BID);
        repository.save(notification);
    }

    public void feeCharge(final UserAccount account, final BigDecimal amount) {
        final var appUser = account.getUser();
        final var notification = new AppNotification()
                .setTitle("Success fee has been charged")
                .setDetail(String.format("Success fee has been charged %s from your credit balance. Your credit balance is - %s", amount, account.getAmount()))
                .setAppUser(appUser)
                .setData(Map.of("id", "" + appUser.getId()))
                .setType(NotificationType.FEE_CHARGE);
        repository.save(notification);
    }

    public void topUp(final UserAccount account, final BigDecimal amount) {
        final var appUser = account.getUser();
        final var notification = new AppNotification()
                .setTitle("Your credit is top up successfully")
                .setDetail(String.format("Your credit is top up successfully %s$. Your credit balance is - %s", amount, account.getAmount()))
                .setAppUser(appUser)
                .setData(Map.of("id", "" + appUser.getId()))
                .setType(NotificationType.TOP_UP_SUCCESS);
        repository.save(notification);
    }

    public void higherBid(BidPrice bidPrice) {
        var product = bidPrice.getProduct();
        // send only to latest bid
        var highestBid = product.getHighestBid();

        final var notification = new AppNotification()
                .setTitle("Other people bid higher than you")
                .setDetail("Other people bid higher than you")
                .setAppUser(highestBid.getCreatedBy())
                .setImage(product.getBgImage())
                .setData(Map.of("id", "" + product.getId()))
                .setType(NotificationType.HIGHER_BID);
        repository.save(notification);
    }
}
