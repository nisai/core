package com.nisai.auction.basic.notification.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.nisai.auction.basic.notification.listener.NotificationListener;
import com.nisai.auction.persistence.domain.VersionEntity;
import com.nisai.auction.persistence.utils.StringMapConverter;
import com.nisai.auction.user.authentication.domain.AppUser;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Map;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Filter(name = "userFilter", condition = "app_user_id = :id")
@Filter(name = "readFilter", condition = "app_user_id = :id")
@Table(name = "notification")
@EntityListeners(NotificationListener.class)
public class AppNotification extends VersionEntity {

    @Column(updatable = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    LocalDateTime createdAt = LocalDateTime.now();

    @Column(nullable = false)
    String title;

    @Enumerated(EnumType.STRING)
    NotificationType type;

    String detail;

    String image;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    AppUser appUser;

    @Enumerated(EnumType.STRING)
    NotificationStatus status = NotificationStatus.NEW;

    @Column(columnDefinition = "text")
    @Convert(converter = StringMapConverter.class)
    Map<String, String> data;
}
