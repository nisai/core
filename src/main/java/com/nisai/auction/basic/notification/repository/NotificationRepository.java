package com.nisai.auction.basic.notification.repository;

import com.nisai.auction.basic.notification.domain.AppNotification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<AppNotification, Long> {
}
