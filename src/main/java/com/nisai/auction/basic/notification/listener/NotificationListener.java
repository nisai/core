package com.nisai.auction.basic.notification.listener;

import com.nisai.auction.appconfiguration.utils.AutowiringHelper;
import com.nisai.auction.basic.notification.service.FirebaseService;
import com.nisai.auction.basic.notification.domain.AppNotification;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.persistence.PostPersist;

public class NotificationListener {

    @PostPersist
    public void postPersist(AppNotification appNotification) {
        TransactionSynchronizationManager.registerSynchronization(new TransactionAdapter(appNotification));
    }

    @RequiredArgsConstructor
    private static class TransactionAdapter extends TransactionSynchronizationAdapter {

        private final AppNotification appNotification;

        @Autowired
        private FirebaseService messagingService;

        @Override
        public void afterCommit() {
            AutowiringHelper.autowire(this, messagingService);
            messagingService.sendNotification(appNotification);
        }
    }
}
