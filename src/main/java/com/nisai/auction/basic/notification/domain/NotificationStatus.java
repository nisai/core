package com.nisai.auction.basic.notification.domain;

public enum NotificationStatus {
    NEW,
    SEEN,
    DELETED
}
