package com.nisai.auction.organization.account.domain;

import com.nisai.auction.graphql.annotation.GQLIgnoreGenerate;
import com.nisai.auction.organization.account.exception.InsufficientBalanceException;
import com.nisai.auction.persistence.domain.VersionEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@GQLIgnoreGenerate
public class GLAccount extends VersionEntity {

    @Column(unique = true, updatable = false)
    private String code;

    private String name;

    private BigDecimal amount = BigDecimal.ZERO;

    private String details;

    public GLAccountTransaction debit(BigDecimal amount) {
        if (this.amount.compareTo(amount) < 0) {
            throw new InsufficientBalanceException();
        }
        this.amount = this.amount.subtract(amount);
        return new GLAccountTransaction()
                .setTransactionType(TransactionType.DEBIT)
                .setAmount(amount)
                .setGlAccount(this);
    }

    public GLAccountTransaction credit(BigDecimal amount) {
        this.amount = this.amount.add(amount);
        return new GLAccountTransaction()
                .setTransactionType(TransactionType.CREDIT)
                .setAmount(amount)
                .setGlAccount(this);
    }
}
