package com.nisai.auction.organization.account.service;

import com.nisai.auction.organization.account.domain.GLAccount;
import com.nisai.auction.organization.account.domain.PaymentMethod;
import com.nisai.auction.organization.account.repository.GLAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GLAccountService {
    private static final String CASH_GL_CODE = "100001";
    private static final String PI_PAY_GL_CODE = "100003";
    private static final String WING_GL_CODE = "100004";
    private static final String MAIN_GL_CODE = "100009";
    private static final String INCOME_GL_CODE = "100010";
    private static final String USER_GL_CODE = "100011";

    /**
     * About Gl account:
     * User_Gl = PiPay_Gl + Wing_Gl + Cash_Gl
     * Main_Gl = User_Gl + Incoming_Gl
     */

    @Autowired
    private GLAccountRepository repository;

    public GLAccount getGlAccount(PaymentMethod method) {
        switch (method) {
            case WING:
                return getGLByCode(WING_GL_CODE);
            case PI_PAY:
                return getGLByCode(PI_PAY_GL_CODE);
            default:
                return getGLByCode(CASH_GL_CODE);
        }
    }

    public GLAccount getMainGLAccount() {
        return getGLByCode(MAIN_GL_CODE);
    }

    public GLAccount getUserGLAccount() {
        return getGLByCode(USER_GL_CODE);
    }

    public GLAccount getIncomeGLAccount() {
        return getGLByCode(INCOME_GL_CODE);
    }

    private GLAccount getGLByCode(String code) {
        return repository.findByCode(code).orElseThrow(() -> new RuntimeException("GLAccount for " + code + " is not defined"));
    }
}
