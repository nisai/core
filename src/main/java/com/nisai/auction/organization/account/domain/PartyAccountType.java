package com.nisai.auction.organization.account.domain;

public enum PartyAccountType {
    GL_ACCOUNT,
    USER_ACCOUNT,
    DEPOSIT,
    WITHDRAW
}
