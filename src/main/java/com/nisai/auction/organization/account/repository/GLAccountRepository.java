package com.nisai.auction.organization.account.repository;

import com.nisai.auction.organization.account.domain.GLAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GLAccountRepository extends JpaRepository<GLAccount, Long> {

    Optional<GLAccount> findByCode(final String code);
}
