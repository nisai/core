package com.nisai.auction.organization.account.domain;

import com.introproventures.graphql.jpa.query.annotation.GraphQLIgnore;
import com.nisai.auction.graphql.annotation.GQLIgnoreGenerate;
import com.nisai.auction.persistence.domain.AuditingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class GLAccountTransaction extends AuditingEntity {

    @ManyToOne
    @JoinColumn
    @GQLIgnoreGenerate
    @GraphQLIgnore
    private GLAccount glAccount;

    private BigDecimal amount = BigDecimal.ZERO;

    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @Enumerated(EnumType.STRING)
    private PartyAccountType partyAccountType;

    private Long partyAccountId;

    private String partyAccountDetail;

    private String transactionDetail;
}
