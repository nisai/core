package com.nisai.auction.organization.account.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InsufficientBalanceException extends ResponseStatusException {
    public InsufficientBalanceException() {
        super(HttpStatus.EXPECTATION_FAILED, "insufficient amount");
    }
}