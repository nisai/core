package com.nisai.auction.organization.account.repository;

import com.nisai.auction.organization.account.domain.GLAccountTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GLAccountTransactionRepository extends JpaRepository<GLAccountTransaction, Long> {
}
