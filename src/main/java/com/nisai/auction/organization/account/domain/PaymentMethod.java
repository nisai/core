package com.nisai.auction.organization.account.domain;

public enum PaymentMethod {
    CASH,
    WING,
    PI_PAY
}
