package com.nisai.auction.organization.account.domain;

public enum TransactionType {
    DEBIT,
    CREDIT
}
