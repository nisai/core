package com.nisai.auction.organization.account.controller;

import com.nisai.auction.organization.account.domain.GLAccount;
import com.nisai.auction.organization.account.service.GLAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glaccount")
public class GLAccountController {

    @Autowired
    private GLAccountService service;

    @GetMapping("/main")
    public GLAccount getMainGl(){
        return service.getMainGLAccount();
    }
}
