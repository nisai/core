package com.nisai.auction.organization.contact.repository;

import com.nisai.auction.organization.contact.domain.FeedBack;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeedBackRepository extends JpaRepository<FeedBack, Long> {
}
