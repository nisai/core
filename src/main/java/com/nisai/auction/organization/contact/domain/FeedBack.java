package com.nisai.auction.organization.contact.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.nisai.auction.persistence.domain.ExtendedEntity;
import com.nisai.auction.persistence.domain.VersionEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Email;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class FeedBack extends VersionEntity {

    private String name;

    @Email
    private String email;

    private String tel;

    private String subject;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime time;

    @Column(columnDefinition = "text")
    private String content;
}
