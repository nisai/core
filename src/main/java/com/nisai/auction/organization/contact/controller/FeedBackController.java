package com.nisai.auction.organization.contact.controller;

import com.nisai.auction.organization.contact.domain.FeedBack;
import com.nisai.auction.organization.contact.repository.FeedBackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/feedback")
public class FeedBackController {

    @Autowired
    private FeedBackRepository repository;

    @PostMapping
    public boolean saveFeedback(@RequestBody final FeedBack feedBack){
        feedBack.setTime(LocalDateTime.now());
        repository.save(feedBack);
        return true;
    }
}
