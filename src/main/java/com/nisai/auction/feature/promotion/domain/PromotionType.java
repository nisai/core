package com.nisai.auction.feature.promotion.domain;

public enum  PromotionType {
    URGENT,
    ADVERTISEMENT
}
