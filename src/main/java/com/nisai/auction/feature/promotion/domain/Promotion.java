package com.nisai.auction.feature.promotion.domain;

import com.nisai.auction.persistence.domain.VersionEntity;
import com.nisai.auction.persistence.utils.JsonObjectConverter;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Map;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class Promotion extends VersionEntity {

    @Column(nullable = false)
    private String name;

    private String image;

    @Enumerated(EnumType.STRING)
    private PromotionType type = PromotionType.ADVERTISEMENT;

    @Column(columnDefinition = "text")
    private String detail;

    private int duration = 1;

    private BigDecimal fee = BigDecimal.ZERO;

    private boolean active;

    @Column(columnDefinition = "text")
    @Convert(converter = JsonObjectConverter.class)
    private Map<String,Object> json;
}
