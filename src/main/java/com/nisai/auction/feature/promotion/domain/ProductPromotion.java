package com.nisai.auction.feature.promotion.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.nisai.auction.feature.payment.domain.PaymentMethod;
import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.graphql.annotation.GQLIgnoreGenerate;
import com.nisai.auction.persistence.domain.AuditingEntity;
import com.nisai.auction.user.account.domain.UserAccountTransaction;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class ProductPromotion extends AuditingEntity {

    @ManyToOne(optional = false)
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "promotion_id")
    private Promotion promotion;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime promotionStartAt = LocalDateTime.now();

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime promotionFinishAt;

    @Enumerated(EnumType.STRING)
    private PaymentMethod paymentMethod;

    @OneToOne
    @JoinColumn(name = "transaction_id")
    private UserAccountTransaction transaction;

    @Enumerated(EnumType.STRING)
    @GQLIgnoreGenerate
    private PromotionStatus status = PromotionStatus.ORDERED;
}
