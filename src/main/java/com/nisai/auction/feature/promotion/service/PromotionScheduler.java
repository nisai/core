package com.nisai.auction.feature.promotion.service;

import com.nisai.auction.feature.product.domain.ProductLabel;
import com.nisai.auction.feature.product.repository.ProductRepository;
import com.nisai.auction.feature.promotion.domain.PromotionStatus;
import com.nisai.auction.feature.promotion.repository.ProductPromotionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Duration;
import java.time.LocalDateTime;

@Service
@Slf4j
public class PromotionScheduler {

    @Autowired
    private ProductPromotionRepository repository;
    @Autowired
    private ProductRepository productRepository;

    @Transactional
    @Scheduled(cron = "0 * * ? * *")
    public void checkExpiredPromotion() {
        repository.findAllByStatus(PromotionStatus.ON_GOING).forEach(productPromotion -> {
            if (Duration.ofDays(productPromotion.getPromotion().getDuration())
                    .compareTo(Duration.between(productPromotion.getPromotionStartAt(), LocalDateTime.now())) < 0) {
                productPromotion.setStatus(PromotionStatus.EXPIRED);
                productPromotion.setPromotionFinishAt(LocalDateTime.now());
                repository.save(productPromotion);
                var product = productPromotion.getProduct();
                product.setLabel(ProductLabel.NORMAL);
                productRepository.save(product);
            }
        });
    }
}
