package com.nisai.auction.feature.promotion.repository;

import com.nisai.auction.feature.promotion.domain.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface PromotionRepository extends JpaRepository<Promotion, Long> {

    @Query(value = "SELECT SUM(fee) FROM promotion WHERE id IN (SELECT promotion_id FROM product_promotion WHERE product_id = :id)", nativeQuery = true)
    BigDecimal getPromotionPrice(@Param("id") final Long productId);
}
