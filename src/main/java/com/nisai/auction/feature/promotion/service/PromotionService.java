package com.nisai.auction.feature.promotion.service;

import com.nisai.auction.feature.promotion.repository.PromotionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class PromotionService {

    private final PromotionRepository repository;

    public BigDecimal getPromotionFee(final Long productId) {
        try {
            return repository.getPromotionPrice(productId) == null ? BigDecimal.ZERO : repository.getPromotionPrice(productId);
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }
}
