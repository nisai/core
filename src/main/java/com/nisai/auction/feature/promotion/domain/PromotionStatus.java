package com.nisai.auction.feature.promotion.domain;

public enum PromotionStatus {
    ORDERED,
    PAID,
    SCHEDULED,
    ON_GOING,
    EXPIRED
}
