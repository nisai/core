package com.nisai.auction.feature.promotion.repository;

import com.nisai.auction.feature.promotion.domain.ProductPromotion;
import com.nisai.auction.feature.promotion.domain.PromotionStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductPromotionRepository extends JpaRepository<ProductPromotion, Long> {
    List<ProductPromotion> findAllByStatus(final PromotionStatus status);
    List<ProductPromotion> findAllByProductId(final Long id);
    List<ProductPromotion> findAllByProductIdAndStatus(final Long id,final PromotionStatus status);
}
