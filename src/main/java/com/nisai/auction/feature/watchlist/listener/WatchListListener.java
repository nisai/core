package com.nisai.auction.feature.watchlist.listener;

import com.nisai.auction.appconfiguration.utils.ApplicationSecurityContext;
import com.nisai.auction.appconfiguration.utils.AutowiringHelper;
import com.nisai.auction.feature.product.domain.ProductStatus;
import com.nisai.auction.feature.watchlist.domain.WatchList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.PrePersist;

@Slf4j
public class WatchListListener {

    @Autowired
    private ApplicationSecurityContext context;

    @PrePersist
    private void prePersist(WatchList watchList) {
        AutowiringHelper.autowire(this, context);
        if (watchList.getProduct().getStatus() != ProductStatus.BID_IN_PROGRESS) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "product already expired");
        }
        if (watchList.getProduct().getCreatedBy().equals(context.authenticatedUser())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "cannot add own item to watch list");
        }
    }
}
