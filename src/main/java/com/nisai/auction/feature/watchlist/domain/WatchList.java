package com.nisai.auction.feature.watchlist.domain;

import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.feature.watchlist.listener.WatchListListener;
import com.nisai.auction.persistence.domain.AuditingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"created_by_id", "product_id"}))
@Filter(name = "userFilter", condition = "created_by_id = :id")
@FilterDef(name = "readFilter",
        defaultCondition = "created_by_id = :id",
        parameters = @ParamDef(name = "id", type = "long"))
@Filter(name = "readFilter")
@EntityListeners(WatchListListener.class)
public class WatchList extends AuditingEntity {

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @Enumerated(EnumType.STRING)
    private WatchStatus status = WatchStatus.ACTIVE;
}
