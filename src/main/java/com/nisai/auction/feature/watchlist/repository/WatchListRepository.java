package com.nisai.auction.feature.watchlist.repository;

import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.feature.watchlist.domain.WatchList;
import com.nisai.auction.feature.watchlist.domain.WatchStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WatchListRepository extends JpaRepository<WatchList, Long> {
    List<WatchList> findAllByProductAndStatus(Product product, WatchStatus status);
}
