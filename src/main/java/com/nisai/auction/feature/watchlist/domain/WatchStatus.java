package com.nisai.auction.feature.watchlist.domain;

public enum WatchStatus {
    ACTIVE,
    EXPIRED,
    DELETED,
}
