package com.nisai.auction.feature.partnerapi.controller;

import com.nisai.auction.feature.payment.service.WingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/partner-api")
public class PartnerAPIController {

    private static final String WING_PARTNER_ID = "iwingcambodia";
    private static final String WING_PARTNER_ACCESS_KEY = "iwingpT7rzL5DT/EDcDt9xGUVtFqAO7MHQWY471JmBayEYKk";

    private final WingService wingService;

    public PartnerAPIController(final WingService wingService) {
        this.wingService = wingService;
    }

    @GetMapping(value = "/wing")
    public ResponseEntity<Map<String, Object>> wing(
            @RequestHeader(value = "partner_id", required = false) final String partnerId,
            @RequestHeader(value = "partner_access_key", required = false) final String partnerAccessKey,
            @RequestParam(name = "transaction_id", required = false) final String transactionId) {

        var data = new HashMap<String, Object>();
        try {
            if (Objects.equals(partnerId, WING_PARTNER_ID)
                    && Objects.equals(partnerAccessKey, WING_PARTNER_ACCESS_KEY)) {

                final var model = wingService.findByTransaction(transactionId).orElseThrow();
                data.put("response_code", "success");
                data.put("status_code", "200");
                data.put("response_message", "Transaction success");
                data.put("customer_id", model.getWingAccount());
                data.put("customer_name", model.getCustomerName());
                data.put("customer_amount", model.getAmount());
                data.put("customer_currency", model.getCurrency());
                data.put("partner_txn_id", model.getId());
                data.put("transaction_id", model.getTransId());
                data.put("transaction_date", model.getTransactionDate());
                return new ResponseEntity<>(data, HttpStatus.OK);
            }
        } catch (Exception exception) {

        }
        data.put("response_code", "failed");
        data.put("status_code", "404");
        return new ResponseEntity<>(data, HttpStatus.NOT_FOUND);
    }
}
