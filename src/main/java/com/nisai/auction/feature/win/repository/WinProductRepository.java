package com.nisai.auction.feature.win.repository;

import com.nisai.auction.feature.win.domain.WinProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WinProductRepository extends JpaRepository<WinProduct, Long> {
}
