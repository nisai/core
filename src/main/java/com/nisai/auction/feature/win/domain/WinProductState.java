package com.nisai.auction.feature.win.domain;

public enum WinProductState {
    PENDING,
    ORDERED
}
