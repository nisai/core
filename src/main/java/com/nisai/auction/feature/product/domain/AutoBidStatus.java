package com.nisai.auction.feature.product.domain;

public enum AutoBidStatus {
    ACTIVE,
    LOST,
    INACTIVE
}
