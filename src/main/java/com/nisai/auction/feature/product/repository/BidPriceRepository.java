package com.nisai.auction.feature.product.repository;

import com.nisai.auction.feature.product.domain.BidPrice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidPriceRepository extends JpaRepository<BidPrice, Long> {
}
