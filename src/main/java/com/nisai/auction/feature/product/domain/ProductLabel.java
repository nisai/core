package com.nisai.auction.feature.product.domain;

public enum ProductLabel {
    NEW,
    URGENT, // ads
    HIGH_LIGHTED, // ads
    R1000,
    NORMAL,
    FINISH_SOON
}
