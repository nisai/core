package com.nisai.auction.feature.product.domain;

public enum Delivery {
    FREE,
    PICK_UP,
    BUYER_ARRANGE,
    SELLER_ARRANGE
}
