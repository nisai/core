package com.nisai.auction.feature.product.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nisai.auction.graphql.annotation.GQLIgnoreGenerate;
import com.nisai.auction.persistence.domain.VersionEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.FilterDef;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@FilterDef(name = "adminFilter", defaultCondition = "1 = 0")
@Accessors(chain = true)
public class Category extends VersionEntity {

    @Column(nullable = false)
    private String name;

    private String logoImage;

    private String banner;

    private int orderNumber = 0;

    private boolean firstLevel;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "parent_id")
    private Category parent;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    @GQLIgnoreGenerate
    @JsonIgnore
    private List<Category> child;

    @PrePersist
    void prePersist() {
        if (parent == null) {
            firstLevel = true;
        }
    }
}
