package com.nisai.auction.feature.product.repository;

import com.nisai.auction.feature.product.domain.Category;
import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.feature.product.domain.ProductStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Page<Product> findAllByStatus(final ProductStatus status, Pageable pageable);

    Page<Product> findAllByCategoryInAndStatus(final List<Category> categories, final ProductStatus status, Pageable pageable);

    Page<Product> findAllByNameLikeOrDescriptionLike(final String name, final String description, final Pageable pageable);
}
