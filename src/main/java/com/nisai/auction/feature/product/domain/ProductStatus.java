package com.nisai.auction.feature.product.domain;

public enum ProductStatus {
    SCHEDULED,
    BID_IN_PROGRESS,
    BID_FINISHED,
    ORDERED,
    SOLD,
    EXPIRED,
    DELETED
}
