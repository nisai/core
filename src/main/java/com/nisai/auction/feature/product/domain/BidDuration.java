package com.nisai.auction.feature.product.domain;

import lombok.Getter;

@Getter
public enum BidDuration {
    ONE(1, "one day"),
    THREE(3, "three days"),
    SEVEN(7, "seven days"),
    FOURTEEN(14, "fourteen days"),
    THIRTY(30, "thirty days");

    private int duration;
    private String description;

    BidDuration(int duration, String description) {
        this.duration = duration;
        this.description = description;
    }
}
