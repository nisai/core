package com.nisai.auction.feature.product.service;

import com.nisai.auction.feature.payment.service.PaymentService;
import com.nisai.auction.feature.product.domain.BidPrice;
import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.feature.product.repository.ProductRepository;
import com.nisai.auction.feature.promotion.service.PromotionService;
import com.nisai.auction.feature.watchlist.domain.WatchStatus;
import com.nisai.auction.feature.watchlist.repository.WatchListRepository;
import com.nisai.auction.persistence.exception.ResourceNotFoundException;
import com.nisai.auction.basic.notification.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository repository;
    private final ProductFeeService feeService;
    private final PromotionService promotionService;
    private final WatchListRepository watchListRepository;
    private final NotificationService notificationService;
    private final PaymentService paymentService;

    @Async
    public void updateHighestBid(final BidPrice bidPrice) {
        final var product = bidPrice.getProduct();
        if (product.getReservePrice() != null && bidPrice.getAmount().compareTo(product.getReservePrice()) > 0) {
            product.setReserveMet(true);
        }
        product.setHighestBid(bidPrice);
        repository.save(product);
    }

    @Async
    public void sendNotificationToWatchList(Product product) {
        watchListRepository.findAllByProductAndStatus(product, WatchStatus.ACTIVE).forEach(notificationService::favBid);
    }

    public Map<String, Object> getPriceList(final Long productId) {
        BigDecimal durationFee = BigDecimal.ZERO;
        BigDecimal subtitleFee = BigDecimal.ZERO;
        BigDecimal promotionFee = promotionService.getPromotionFee(productId);

        final var product = repository.findById(productId).orElseThrow(() -> new ResourceNotFoundException(Product.class, productId));
        if (product.getBidDuration().getDuration() > 7) {
            durationFee = feeService.getDurationFee(product.getBidDuration());
        }
        if (product.getSubTitle() != null) {
            subtitleFee = feeService.getSubTitleFee(product);
        }
        return Map.of(
                "duration_fee", durationFee,
                "subtitle_fee", subtitleFee,
                "promotion_fee", promotionFee);
    }

    public void relistProduct(final Long id) {
        var product = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(Product.class, id));
        paymentService.payForProduct(id);
        watchListRepository.findAllByProductAndStatus(product, WatchStatus.ACTIVE).forEach(notificationService::favRelist);
    }
}
