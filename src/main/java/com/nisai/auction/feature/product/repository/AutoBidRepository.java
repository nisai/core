package com.nisai.auction.feature.product.repository;

import com.nisai.auction.feature.product.domain.AutoBid;
import com.nisai.auction.feature.product.domain.AutoBidStatus;
import com.nisai.auction.feature.product.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AutoBidRepository extends JpaRepository<AutoBid, Long> {

    Optional<AutoBid> findFirstByProductAndIdNotAndStatusOrderByAmountDesc(final Product product, final Long id, final AutoBidStatus status);

    Optional<AutoBid> findFirstByProductAndStatusOrderByAmountDesc(final Product product, final AutoBidStatus status);
}
