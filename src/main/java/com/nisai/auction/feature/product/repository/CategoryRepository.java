package com.nisai.auction.feature.product.repository;

import com.nisai.auction.feature.product.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    List<Category> findAllByParent(final Category category);
}
