package com.nisai.auction.feature.product.domain;

import com.nisai.auction.feature.product.listener.AutoBidListener;
import com.nisai.auction.graphql.annotation.GQLIgnoreGenerate;
import com.nisai.auction.persistence.domain.AuditingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Filter(name = "userFilter", condition = "created_by = :id")
@EntityListeners(AutoBidListener.class)
public class AutoBid extends AuditingEntity {

    @ManyToOne
    @JoinColumn(nullable = false)
    private Product product;

    @Column(unique = true, nullable = false)
    private BigDecimal amount;

    @GQLIgnoreGenerate
    @Enumerated(EnumType.STRING)
    private AutoBidStatus status = AutoBidStatus.ACTIVE;

    private String detail;
}
