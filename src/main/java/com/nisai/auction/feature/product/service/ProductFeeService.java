package com.nisai.auction.feature.product.service;

import com.nisai.auction.basic.config.domain.SystemConfigEnum;
import com.nisai.auction.basic.config.service.SystemConfigService;
import com.nisai.auction.feature.payment.domain.PaymentFor;
import com.nisai.auction.feature.payment.service.PayService;
import com.nisai.auction.feature.product.domain.BidDuration;
import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.user.account.service.UserAccountService;
import com.nisai.auction.feature.win.domain.WinProduct;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@AllArgsConstructor
public class ProductFeeService {

    private final SystemConfigService configService;
    private final UserAccountService accountService;
    private final PayService payService;

    public BigDecimal getProductFee(Product product) {
        return getDurationFee(product.getBidDuration()).add(getSubTitleFee(product));
    }

    public BigDecimal getDurationFee(BidDuration duration) {
        switch (duration) {
            case FOURTEEN:
                return configService.getValueAsBigDecimal(SystemConfigEnum.BID_14_DAY_FEE);
            case THIRTY:
                return configService.getValueAsBigDecimal(SystemConfigEnum.BID_30_DAY_FEE);
            default:
                return BigDecimal.ZERO;
        }
    }

    public BigDecimal getSubTitleFee(Product product) {
        if (product.getSubTitle() != null && !product.getSubTitle().isBlank()) {
            return configService.getValueAsBigDecimal(SystemConfigEnum.SUB_TITLE_FEE);
        }
        return BigDecimal.ZERO;
    }

    public void chargeCommissionFee(WinProduct winProduct) {
        var feeRate = configService.getValueAsBigDecimal(SystemConfigEnum.COMMISSION_FEE);
        if (feeRate == null) return;
        var rate = feeRate.divide(new BigDecimal("100"));
        var fee = winProduct.getBidPrice().getAmount().multiply(rate);
        var owner = winProduct.getProduct().getCreatedBy();
        var account = accountService.getByUser(owner);
        payService.pay(account, fee, PaymentFor.COMMISSION, "commission for product sale");
    }
}
