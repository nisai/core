package com.nisai.auction.feature.product.domain;

import com.nisai.auction.feature.product.listener.BidPriceListener;
import com.nisai.auction.persistence.domain.AuditingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@EntityListeners(BidPriceListener.class)
public class BidPrice extends AuditingEntity {

    @ManyToOne
    @JoinColumn(name = "product_id", updatable = false, nullable = false)
    private Product product;

    @Min(0)
    @Column(nullable = false)
    private BigDecimal amount;

    public Long getCreatedById() {
        return getCreatedBy().getId();
    }

    public Long getProductCreatedById() {
        return product.getCreatedBy().getId();
    }
}
