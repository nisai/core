package com.nisai.auction.feature.product.service;

import com.nisai.auction.basic.config.domain.SystemConfigEnum;
import com.nisai.auction.basic.config.service.SystemConfigService;
import com.nisai.auction.feature.product.domain.ProductStatus;
import com.nisai.auction.feature.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductScheduler {

    private final SystemConfigService configService;
    private final ProductRepository repository;
    private final ProductSchedulerService service;

    @Scheduled(cron = "0 * * ? * *") // every minute
    public void productLabelChanger() {
        final var newDuration = configService.getValueAsLong(SystemConfigEnum.LABEL_NEW_DURATION);
        final var finishSoonDuration = configService.getValueAsLong(SystemConfigEnum.LABEL_FINISH_SOON_DURATION);
        var pageable = PageRequest.of(0, 200, Sort.by(Sort.Direction.ASC, "id"));
        var products = repository.findAllByStatus(ProductStatus.BID_IN_PROGRESS, pageable);
        while (products.getTotalElements() > 0) {
            products.forEach(product -> service.updateProduct(product, newDuration, finishSoonDuration));
            if (products.hasNext()) {
                products = repository.findAllByStatus(ProductStatus.BID_IN_PROGRESS, products.nextPageable());
            } else {
                break;
            }
        }

    }

}