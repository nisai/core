package com.nisai.auction.feature.product.listener;

import com.nisai.auction.appconfiguration.utils.AutowiringHelper;
import com.nisai.auction.feature.product.domain.BidDuration;
import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.feature.product.domain.ProductLabel;
import com.nisai.auction.feature.product.domain.ProductStatus;
import com.nisai.auction.user.account.service.UserAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.StringUtils;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import java.math.BigDecimal;

@Slf4j
public class ProductListener {

    @PrePersist
    private void prePersist(Product product) {
        if (product.getBidDuration().equals(BidDuration.ONE)
                || product.getBidDuration().equals(BidDuration.THREE)
                || product.getBidDuration().equals(BidDuration.SEVEN)) {
            if (!StringUtils.hasText(product.getSubTitle())) {
                product.setStatus(ProductStatus.BID_IN_PROGRESS);
            }
        }
        if (product.getOriginalPrice() == null) {
            product.setOriginalPrice(BigDecimal.ZERO);
        }
        if (BigDecimal.ZERO.equals(product.getOriginalPrice())) {
            product.setLabel(ProductLabel.R1000);
        }
    }

    @PostLoad
    private void postLoad(Product product) {
        product.setPreviousStatus(product.getStatus());
    }

    @PostPersist
    @PostUpdate
    private void postPersist(Product product) {
        TransactionSynchronizationManager.registerSynchronization(new TxSyncAdapter(product));
    }

    static class TxSyncAdapter extends TransactionSynchronizationAdapter {

        @Autowired
        private UserAccountService service;

        private final Product product;

        public TxSyncAdapter(Product product) {
            this.product = product;
        }

        @Override
        public void afterCommit() {
            AutowiringHelper.autowire(this, service);
            service.updateUserSummary(product);
        }
    }
}
