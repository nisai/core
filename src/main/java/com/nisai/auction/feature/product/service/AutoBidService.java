package com.nisai.auction.feature.product.service;

import com.nisai.auction.feature.product.domain.AutoBid;
import com.nisai.auction.feature.product.domain.AutoBidStatus;
import com.nisai.auction.feature.product.repository.AutoBidRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
@AllArgsConstructor
public class AutoBidService {

    private final AutoBidRepository repository;

    @Async
    @Transactional
    public void checkStatus(final AutoBid autoBid) {
        var result = repository.findFirstByProductAndIdNotAndStatusOrderByAmountDesc(autoBid.getProduct(), autoBid.getId(), AutoBidStatus.ACTIVE);
        if (result.isPresent()) {
            var topAutoBid = result.get();
            if (autoBid.getAmount().compareTo(topAutoBid.getAmount()) < 0) {
                autoBid.setStatus(AutoBidStatus.LOST);
                autoBid.setDetail("your auto bid has been lost, there are higher amount of auto bid");
                repository.save(autoBid);
                log.warn("your auto bid has been lost, there are higher amount of auto bid");
            } else {
                topAutoBid.setStatus(AutoBidStatus.LOST);
                autoBid.setDetail("your auto bid has been lost, there are higher amount of auto bid");
                repository.save(topAutoBid);
                log.warn("auto bid with id:{} has lost, there are higher amount of auto bid", topAutoBid.getId());
            }
        }
    }
}
