package com.nisai.auction.feature.product.service;

import com.nisai.auction.feature.product.domain.AutoBidStatus;
import com.nisai.auction.feature.product.domain.BidPrice;
import com.nisai.auction.feature.product.repository.AutoBidRepository;
import com.nisai.auction.feature.product.repository.BidPriceRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Slf4j
@Service
@AllArgsConstructor
public class BidPriceService {

    private static final BigDecimal INCREMENT_AMOUNT = new BigDecimal("0.1");

    private final AutoBidRepository autoBidRepository;
    private final BidPriceRepository repository;

    @Async
    public void checkAutoBid(final BidPrice bidPrice) {
        final var product = bidPrice.getProduct();
        final var bidAmount = bidPrice.getAmount();
        var result = autoBidRepository.findFirstByProductAndStatusOrderByAmountDesc(product, AutoBidStatus.ACTIVE);
        if (result.isPresent()) {
            var autoBid = result.get();
            // if own bid finish
            var bidPriceCreator = bidPrice.getCreatedBy() == null ? null : bidPrice.getCreatedBy().getId();
            var autoBidCreator = autoBid.getCreatedBy() == null ? null : autoBid.getCreatedBy().getId();
            if (bidPriceCreator == null || autoBidCreator == null || bidPriceCreator.equals(autoBidCreator)) {
                return;
            }
            // if not
            var autoBidAmount = autoBid.getAmount();
            var newBidAmount = bidAmount.add(INCREMENT_AMOUNT);
            if (autoBidAmount.compareTo(bidAmount) <= 0 || newBidAmount.compareTo(autoBidAmount) >= 0) {
                // auto bid has lost
                autoBid.setStatus(AutoBidStatus.INACTIVE);
                autoBidRepository.save(autoBid);
                log.info("auto bid with id:{} has lost", autoBid.getId());
                return;
            }

            var newBidPrice = new BidPrice().setProduct(product).setAmount(newBidAmount);
            newBidPrice.setCreatedBy(autoBid.getCreatedBy());
            newBidPrice.setCreatedAt(LocalDateTime.now());
            repository.save(newBidPrice);
        }
    }
}
