package com.nisai.auction.feature.product.domain;

public enum ProductPayment {
    CASH,
    ABA,
    ACLEDA,
    WING,
    PIPAY,
    OTHER
}
