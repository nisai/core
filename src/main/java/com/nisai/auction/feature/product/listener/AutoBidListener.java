package com.nisai.auction.feature.product.listener;

import com.nisai.auction.appconfiguration.utils.AutowiringHelper;
import com.nisai.auction.exception.GQLException;
import com.nisai.auction.exception.GQLExceptionCode;
import com.nisai.auction.feature.product.domain.AutoBid;
import com.nisai.auction.feature.product.domain.AutoBidStatus;
import com.nisai.auction.feature.product.repository.AutoBidRepository;
import com.nisai.auction.feature.product.service.AutoBidService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

@Slf4j
public class AutoBidListener {

    @Autowired
    private AutoBidRepository repository;

    @PrePersist
    private void prePersist(final AutoBid autoBid) {
        AutowiringHelper.autowire(this, repository);
        repository.findFirstByProductAndStatusOrderByAmountDesc(autoBid.getProduct(), AutoBidStatus.ACTIVE)
                .ifPresent(topAutoBid -> {
                    if (autoBid.getAmount().compareTo(topAutoBid.getAmount()) == 0) {
                        throw new GQLException("there are auto bid with same price, please auto bid again", GQLExceptionCode.BAD_REQUEST);
                    }
                });
    }

    @PostPersist
    private void postPersist(final AutoBid autoBid) {
        TransactionSynchronizationManager.registerSynchronization(new AMLTransactionSynchronizationAdapter(autoBid));
    }

    static class AMLTransactionSynchronizationAdapter extends TransactionSynchronizationAdapter {

        @Autowired
        private AutoBidService service;

        private final AutoBid autoBid;

        AMLTransactionSynchronizationAdapter(final AutoBid autoBid) {
            this.autoBid = autoBid;
        }

        @Override
        public void afterCommit() {
            AutowiringHelper.autowire(this, service);
            service.checkStatus(autoBid);
        }
    }
}
