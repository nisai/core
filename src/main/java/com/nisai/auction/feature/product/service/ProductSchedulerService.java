package com.nisai.auction.feature.product.service;

import com.nisai.auction.feature.product.domain.BidPrice;
import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.feature.product.domain.ProductLabel;
import com.nisai.auction.feature.product.domain.ProductStatus;
import com.nisai.auction.feature.product.repository.ProductRepository;
import com.nisai.auction.feature.watchlist.domain.WatchStatus;
import com.nisai.auction.feature.watchlist.repository.WatchListRepository;
import com.nisai.auction.feature.lose.domain.LoseProduct;
import com.nisai.auction.feature.lose.repository.LoseProductRepository;
import com.nisai.auction.basic.notification.service.NotificationService;
import com.nisai.auction.feature.win.domain.WinProduct;
import com.nisai.auction.feature.win.domain.WinProductState;
import com.nisai.auction.feature.win.repository.WinProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductSchedulerService {

    private final ProductRepository repository;
    private final WinProductRepository winProductRepository;
    private final LoseProductRepository loseProductRepository;
    private final ProductFeeService feeService;
    private final NotificationService notificationService;
    private final WatchListRepository watchListRepository;

    @Transactional
    public void updateProduct(final Product product, final Long finishSoonDuration, final Long newDuration) {
        if (product.getExpectFinishAt().isBefore(LocalDateTime.now())) {
            product.setBidFinishAt(LocalDateTime.now());
            if (product.isReserveMet()) {
                addProductToWinnerLoser(product);
            } else {
                product.setStatus(ProductStatus.EXPIRED);
                notificationService.bidFinishReserveNotMet(product);
            }
            repository.save(product);
        } else {
            var finishTime = product.getExpectFinishAt();
            var now = LocalDateTime.now();
            if (finishTime.minusDays(1).toLocalDate().equals(LocalDate.now())) {
                if (finishTime.getHour() == now.getHour() && finishTime.getMinute() == now.getMinute()) {
                    // send notification
                    sendNotificationToFavorite(product);
                }
            }
            switch (product.getLabel()) {
                case NEW:
                    if (Duration.between(product.getBidStartAt(), LocalDateTime.now())
                            .compareTo(Duration.ofHours(newDuration)) > 0) {
                        product.setLabel(ProductLabel.NORMAL);
                        repository.save(product);
                        break;
                    }
                case R1000:
                case NORMAL:
                    if (Duration.between(LocalDateTime.now(), product.getExpectFinishAt())
                            .compareTo(Duration.ofHours(finishSoonDuration)) < 0) {
                        product.setLabel(ProductLabel.FINISH_SOON);
                        repository.save(product);
                        break;
                    }
            }
        }
    }

    private void sendNotificationToFavorite(Product product) {
        watchListRepository.findAllByProductAndStatus(product, WatchStatus.ACTIVE).forEach(notificationService::favoriteFinish24h);
        product.getBidPrice().forEach(notificationService::bidFinish24h);
    }

    private void addProductToWinnerLoser(final Product product) {
        BidPrice winBid = product.getBidPrice().get(0);
        var bidList = product.getBidPrice();
        for (BidPrice bidPrice : bidList) {
            if (bidPrice.getAmount().compareTo(winBid.getAmount()) > 0) {
                winBid = bidPrice;
            }
        }
        if (bidList.remove(winBid)) {
            if (!bidList.isEmpty()) {
                addProductToLoser(bidList);
            }
        }
        var winner = winBid.getCreatedBy();
        var winProduct = new WinProduct().setProduct(product)
                .setState(WinProductState.ORDERED)
                .setBidPrice(winBid)
                .setUser(winner);
        winProductRepository.save(winProduct);
        // deduct fee from winner account
        feeService.chargeCommissionFee(winProduct);
        product.setWinBid(winBid);
        product.setStatus(ProductStatus.SOLD);
        sendNotificationToWinner(winProduct);
        sendNotificationToOwner(product);
    }

    private void addProductToLoser(final List<BidPrice> bidList) {
        bidList.forEach(bidPrice -> loseProductRepository.save(new LoseProduct()
                .setProduct(bidPrice.getProduct())
                .setBidPrice(bidPrice)
                .setUser(bidPrice.getCreatedBy())));
    }

    private void sendNotificationToWinner(final WinProduct product) {
        notificationService.bidWin(product);
    }

    private void sendNotificationToOwner(Product product) {
        notificationService.bidFinishReserveMet(product);
    }
}
