package com.nisai.auction.feature.product.controller;

import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.feature.product.domain.ProductStatus;
import com.nisai.auction.feature.product.repository.CategoryRepository;
import com.nisai.auction.feature.product.repository.ProductRepository;
import com.nisai.auction.feature.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductRepository repository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ProductService productService;

    @GetMapping
    public List<Product> getProductByCategory(@RequestParam Long categoryId,
                                              @RequestParam int page,
                                              @RequestParam int limit) {
        var parentCategory = categoryRepository.findById(categoryId).orElseThrow();
        var childCategory = categoryRepository.findAllByParent(parentCategory);
        childCategory.add(parentCategory);
        return repository.findAllByCategoryInAndStatus(childCategory, ProductStatus.BID_IN_PROGRESS, PageRequest.of(page, limit, Sort.by("createdAt"))).getContent();
    }

    @PostMapping("/{id}/relist")
    public Map<String, Object> relist(@PathVariable final Long id) {
        productService.relistProduct(id);
        return Map.of("success", true);
    }

    @GetMapping("/price/{id}")
    public Map<String, Object> getProductPrice(@PathVariable final Long id) {
        return productService.getPriceList(id);
    }
}
