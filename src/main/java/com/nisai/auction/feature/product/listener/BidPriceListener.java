package com.nisai.auction.feature.product.listener;

import com.nisai.auction.appconfiguration.utils.AutowiringHelper;
import com.nisai.auction.basic.notification.service.NotificationService;
import com.nisai.auction.exception.GQLException;
import com.nisai.auction.exception.GQLExceptionCode;
import com.nisai.auction.feature.product.domain.BidPrice;
import com.nisai.auction.feature.product.service.BidPriceService;
import com.nisai.auction.feature.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

public class BidPriceListener {

    @PrePersist
    private void prePersist(final BidPrice bidPrice) {
        if (bidPrice.getAmount().compareTo(bidPrice.getProduct().getOriginalPrice()) <= 0) {
            throw new GQLException("bid price cannot less than product price", GQLExceptionCode.VALIDATION_ERROR);
        }
        if (bidPrice.getCreatedById().equals(bidPrice.getProductCreatedById())) {
            throw new GQLException("cannot bid on your own product", GQLExceptionCode.FORBIDDEN);
        }
        if (bidPrice.getProduct().getHighestBid() == null) {
            return;
        }
        if (bidPrice.getAmount().compareTo(bidPrice.getProduct().getHighestBid().getAmount()) <= 0) {
            throw new GQLException("bid price cannot less than highest bid price", GQLExceptionCode.VALIDATION_ERROR);
        }

    }

    @PostPersist
    private void postPersist(final BidPrice bidPrice) {
        TransactionSynchronizationManager.registerSynchronization(new BidPriceAdapter(bidPrice));
    }

    @RequiredArgsConstructor
    private static class BidPriceAdapter extends TransactionSynchronizationAdapter {

        private final BidPrice bidPrice;

        @Autowired
        private BidPriceService service;
        @Autowired
        private ProductService productService;
        @Autowired
        private NotificationService notificationService;

        @Override
        public void afterCommit() {
            AutowiringHelper.autowire(this, service, productService, notificationService);
            service.checkAutoBid(bidPrice);
            productService.updateHighestBid(bidPrice);
            productService.sendNotificationToWatchList(bidPrice.getProduct());
            notificationService.higherBid(bidPrice);
            notificationService.productBid(bidPrice);
        }
    }
}
