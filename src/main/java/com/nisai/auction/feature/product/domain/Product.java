package com.nisai.auction.feature.product.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.introproventures.graphql.jpa.query.annotation.GraphQLIgnore;
import com.nisai.auction.feature.address.domain.Province;
import com.nisai.auction.feature.product.listener.ProductListener;
import com.nisai.auction.graphql.annotation.GQLIgnoreGenerate;
import com.nisai.auction.persistence.domain.AuditingEntity;
import com.nisai.auction.persistence.utils.StringListConverter;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Where(clause = "status <> 'DELETED'")
@Filter(name = "userFilter", condition = "created_by_id = :id")
@FilterDef(name = "searchFilter", defaultCondition = "status = 'BID_IN_PROGRESS'")
@Filter(name = "searchFilter")
@EntityListeners(ProductListener.class)
@SQLDelete(sql = "update product set status = 'DELETED' where id = ? and version = ?", check = ResultCheckStyle.COUNT)
public class Product extends AuditingEntity {

    @Column(nullable = false)
    private String name;

    private String subTitle;

    @Column(columnDefinition = "text", nullable = false)
    @Convert(converter = StringListConverter.class)
    private List<String> slideImage;

    @Column(columnDefinition = "text")
    @Convert(converter = StringListConverter.class)
    private List<String> tags;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(columnDefinition = "mediumtext")
    private String description;

    private BigDecimal originalPrice;

    private BigDecimal reservePrice;

    @GQLIgnoreGenerate
    private boolean reserveMet;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id")
    @JsonIgnore
    private List<BidPrice> bidPrice;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "highest_bid")
    private BidPrice highestBid;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "win_bid_id")
    private BidPrice winBid;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime bidStartAt = LocalDateTime.now();

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private BidDuration bidDuration = BidDuration.ONE;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime bidFinishAt;

    @Enumerated(EnumType.STRING)
    @ElementCollection
    @CollectionTable
    private Set<Delivery> delivery;

    @Enumerated(EnumType.STRING)
    @ElementCollection
    @CollectionTable
    private Set<ProductPayment> payments;

    private int availableNumber = 1;

    @ManyToOne
    @JoinColumn
    private Province province;

//    private BigDecimal deliveryCost;

    @Enumerated(EnumType.STRING)
    @GQLIgnoreGenerate
    private ProductStatus status = ProductStatus.SCHEDULED;

    @Enumerated(EnumType.STRING)
    @GQLIgnoreGenerate
    private ProductLabel label = ProductLabel.NEW;

    private BigDecimal buyNowPrice;

    @Transient
    private String bgImage;

    @Transient
    @GraphQLIgnore
    private ProductStatus previousStatus;

    public String getBgImage() {
        if (slideImage == null) {
            return "";
        }
        return slideImage.get(0);
    }

    @Transient
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public LocalDateTime getExpectFinishAt() {
        if (bidStartAt == null || bidDuration == null) {
            return null;
        }
        return bidStartAt.plusDays(bidDuration.getDuration());
    }

    @Transient
    public Long getRemainingBidDuration() {
        var finishAt = getExpectFinishAt();
        if (finishAt == null || LocalDateTime.now().isBefore(bidStartAt) || LocalDateTime.now().isAfter(finishAt)) {
            return null;
        }
        return Duration.between(bidStartAt, finishAt).getSeconds();
    }
}
