package com.nisai.auction.feature.comment.listener;

import com.nisai.auction.appconfiguration.utils.ApplicationSecurityContext;
import com.nisai.auction.appconfiguration.utils.AutowiringHelper;
import com.nisai.auction.exception.GQLException;
import com.nisai.auction.feature.comment.domain.Comment;
import com.nisai.auction.basic.notification.service.NotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;

@Slf4j
public class CommentListener {

    @Autowired
    private ApplicationSecurityContext context;

    @PrePersist
    private void prePersist(final Comment comment) {
        if (comment.getProduct().getCreatedBy().equals(comment.getCreatedBy())) {
            comment.setByOwner(true);
        }
    }

    @PreRemove
    private void preUpdate(final Comment comment) {
        AutowiringHelper.autowire(this, context);
        if (!comment.getCreatedBy().equals(context.authenticatedUser())) {
            throw new GQLException("cannot update/delete comment that not belong to you");
        }
    }

    @PostPersist
    private void postPersist(final Comment comment) {
        TransactionSynchronizationManager.registerSynchronization(new TxSynchronizationAdapter(comment));
    }

    @RequiredArgsConstructor
    private static class TxSynchronizationAdapter extends TransactionSynchronizationAdapter {

        private final Comment comment;

        @Autowired
        private NotificationService service;

        @Override
        public void afterCommit() {
            AutowiringHelper.autowire(this, service);
            service.questionAsked(comment.getProduct());
        }
    }
}
