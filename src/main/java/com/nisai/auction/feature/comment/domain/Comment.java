package com.nisai.auction.feature.comment.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nisai.auction.feature.comment.listener.CommentListener;
import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.graphql.annotation.GQLIgnoreGenerate;
import com.nisai.auction.graphql.annotation.GQLIgnoreGenerateType;
import com.nisai.auction.persistence.domain.AuditingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@EntityListeners(CommentListener.class)
@GQLIgnoreGenerate(ignores = GQLIgnoreGenerateType.UPDATE)
public class Comment extends AuditingEntity {

    @NotNull
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Product product;

    @NotNull
    @NotBlank
    private String content;

    @GQLIgnoreGenerate
    private boolean byOwner;
}