package com.nisai.auction.feature.order.repository;

import com.nisai.auction.feature.order.domain.ProductOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductOrderRepository extends JpaRepository<ProductOrder, Long> {
}
