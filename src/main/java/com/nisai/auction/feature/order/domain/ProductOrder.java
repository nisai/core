package com.nisai.auction.feature.order.domain;

import com.nisai.auction.feature.address.domain.Province;
import com.nisai.auction.feature.order.listener.ProductOrderListener;
import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.graphql.annotation.GQLIgnoreGenerate;
import com.nisai.auction.persistence.domain.AuditingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@EntityListeners(ProductOrderListener.class)
public class ProductOrder extends AuditingEntity {

    @OneToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @GQLIgnoreGenerate
    @Column(updatable = false, nullable = false)
    private BigDecimal price = BigDecimal.ZERO;

    @Enumerated(EnumType.STRING)
    @GQLIgnoreGenerate
    private OrderFrom orderFrom = OrderFrom.BUY_NOW;

    @Enumerated(EnumType.STRING)
    @GQLIgnoreGenerate
    private OrderStatus status = OrderStatus.ORDERED;

    @ManyToOne
    @JoinColumn
    private Province province;

    private String location;

    private String note;

    public Long getCreatedById() {
        return getCreatedBy().getId();
    }

    public Long getProductCreatedById() {
        return product.getCreatedBy().getId();
    }
}
