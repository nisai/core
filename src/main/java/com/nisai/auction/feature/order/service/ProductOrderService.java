package com.nisai.auction.feature.order.service;

import com.nisai.auction.basic.config.domain.SystemConfigEnum;
import com.nisai.auction.basic.config.service.SystemConfigService;
import com.nisai.auction.feature.order.domain.ProductOrder;
import com.nisai.auction.feature.product.domain.ProductStatus;
import com.nisai.auction.feature.product.repository.ProductRepository;
import com.nisai.auction.user.account.service.UserAccountService;
import com.nisai.auction.basic.notification.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductOrderService {

    private final ProductRepository repository;
    private final UserAccountService accountService;
    private final SystemConfigService configService;
    private final NotificationService notificationService;

    @Async
    public void orderProduct(final ProductOrder order) {
        var product = order.getProduct();

        // change product status
        product.setStatus(ProductStatus.SOLD);
        product.setBidFinishAt(LocalDateTime.now());
        repository.save(product);

        // commission fee
        var account = accountService.getByUser(product.getCreatedBy());
        var commissionFee = configService.getValueAsBigDecimal(SystemConfigEnum.COMMISSION_FEE);
        accountService.payToIncomeGL(account, commissionFee, SystemConfigEnum.COMMISSION_FEE.name());

        // send Notification
        notificationService.productOrder(order);
    }
}
