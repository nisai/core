package com.nisai.auction.feature.order.domain;

public enum OrderFrom {
    BUY_NOW,
    BID_WIN
}
