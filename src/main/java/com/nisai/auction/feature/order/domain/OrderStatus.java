package com.nisai.auction.feature.order.domain;

public enum OrderStatus {
    ORDERED,
    DELIVERING,
    RECEIVED,
    ACCEPTED
}
