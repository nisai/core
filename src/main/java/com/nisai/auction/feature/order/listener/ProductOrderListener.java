package com.nisai.auction.feature.order.listener;

import com.nisai.auction.appconfiguration.utils.AutowiringHelper;
import com.nisai.auction.exception.GQLException;
import com.nisai.auction.exception.GQLExceptionCode;
import com.nisai.auction.feature.order.domain.ProductOrder;
import com.nisai.auction.feature.order.service.ProductOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import java.math.BigDecimal;

import static com.nisai.auction.feature.order.domain.OrderFrom.BUY_NOW;

public class ProductOrderListener {

    @PrePersist
    private void prePersist(final ProductOrder order) {
        if (order.getProduct().getBuyNowPrice() == null
                || BigDecimal.ZERO.equals(order.getProduct().getBuyNowPrice())) {
            throw new GQLException("this product is not allowed for buy now");
        }
        if (BUY_NOW.equals(order.getOrderFrom())) {
            order.setPrice(order.getProduct().getBuyNowPrice());
        } else {
            order.setPrice(order.getProduct().getWinBid().getAmount());
        }
        if (order.getCreatedById().equals(order.getProductCreatedById())) {
            throw new GQLException("cannot order your own product", GQLExceptionCode.FORBIDDEN);
        }
    }

    @PostPersist
    private void postPersist(final ProductOrder order) {
        TransactionSynchronizationManager.registerSynchronization(new OrderTransactionSynchronizationAdapter(order));
    }

    @RequiredArgsConstructor
    private static class OrderTransactionSynchronizationAdapter extends TransactionSynchronizationAdapter {

        private final ProductOrder order;

        @Autowired
        private ProductOrderService service;

        @Override
        public void afterCommit() {
            AutowiringHelper.autowire(this, service);
            service.orderProduct(order);
        }
    }

}
