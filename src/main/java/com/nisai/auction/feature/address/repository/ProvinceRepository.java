package com.nisai.auction.feature.address.repository;

import com.nisai.auction.feature.address.domain.Province;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProvinceRepository extends JpaRepository<Province, Long> {
}
