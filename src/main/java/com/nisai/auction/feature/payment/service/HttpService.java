package com.nisai.auction.feature.payment.service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

public class HttpService {

    private static final HttpClient CLIENT = HttpClient.newBuilder()
            .connectTimeout(Duration.ofSeconds(10))
            .followRedirects(HttpClient.Redirect.NEVER)
            .build();

    public static HttpResponse<String> get(final String uri, final String token) {
        try {
            return CLIENT.send(request(uri, token).GET().build(), HttpResponse.BodyHandlers.ofString());
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static HttpResponse<String> post(final String uri, final String token) {
        try {
            return CLIENT.send(
                    request(uri, token).POST(HttpRequest.BodyPublishers.noBody()).build(),
                    HttpResponse.BodyHandlers.ofString());
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static HttpRequest.Builder request(final String uri, final String token) {
        return HttpRequest.newBuilder()
                .header("Authorization", "Bearer " + token)
                .uri(URI.create(uri))
                .timeout(Duration.ofSeconds(10));
    }
}
