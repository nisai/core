package com.nisai.auction.feature.payment.domain;

import com.nisai.auction.persistence.domain.PaymentEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Filter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Filter(name = "userFilter", condition = "created_by_id = :id")
public class Wing extends PaymentEntity {

    private String wingAccount;

    private String customerName;

    private boolean successful;
}
/*
 "total": "USD 1.50",
 "remark": "1234321",
 "balance": "USD 288,837.04",
 "customer_fee": "USD 0.00",
 "wing_account": "00001614",
 "biller_name": "SounKolap",
 "transaction_id": "ONL015627",
 "customer_name": "Wing Testing WCX-USD",
 "biller_code": "2000"
 */