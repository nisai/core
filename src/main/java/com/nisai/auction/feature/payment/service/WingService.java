package com.nisai.auction.feature.payment.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nisai.auction.feature.payment.domain.Wing;
import com.nisai.auction.feature.payment.repository.WingRepository;
import com.nisai.auction.persistence.exception.ResourceNotFoundException;
import com.nisai.auction.user.account.service.UserAccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.nisai.auction.organization.account.domain.PaymentMethod.WING;

@Slf4j
@Service
@RequiredArgsConstructor
public class WingService {

    private static final String URL = "https://stageonline.wingmoney.com/wingonlinesdk/";
    private static final String SAND_BOX = "1";
    private static final String USER_NAME = "online.cijd";
    private static final String API_KEY = "927e88094c83c6b69bb4ad219d92e9df91fab3eb010b2516168ee6487da5def1";
    private static final String RETURN = "https://api.khmerauction.com/wing/return";
    private static final String BILL_TILL_RBTN = "0";
    private static final String BILL_TILL_NUMBER = "2000";
    private static final String DEFAULT_WING = "1";

    private final WingRepository repository;
    private final UserAccountService accountService;
    private final ObjectMapper objectMapper;

    public Map<String, String> init(final BigDecimal amount) {
        final var orderId = String.valueOf(System.currentTimeMillis());
        final var wing = new Wing();
        wing.setSuccessful(false);
        wing.setOrderId(orderId);
        wing.setAmount(amount);
        repository.save(wing);
        final var map = new HashMap<String, String>();
        map.put("sandbox", SAND_BOX);
        map.put("amount", amount.toString());
        map.put("username", USER_NAME);
        map.put("rest_api_key", API_KEY);
        map.put("return_url", RETURN);
        map.put("bill_till_rbtn", BILL_TILL_RBTN);
        map.put("bill_till_number", BILL_TILL_NUMBER);
        map.put("default_wing", DEFAULT_WING);
        map.put("remark", orderId);
        map.put("url", URL);
        return map;
    }

    public Map<String, Object> callback(final String encryptedFullResponse) {
        if (!StringUtils.hasText(encryptedFullResponse)) {
            return Map.of("success", false);
        }
        if (!encryptedFullResponse.startsWith("{\"contents\":")) {
            return Map.of("success", false);
        }
        try {
            final var fullNode = objectMapper.readValue(encryptedFullResponse, ObjectNode.class);
            final var encryptedResponse = fullNode.get("contents").asText();
            final var response = WingResponseDecryptor.decryptWithKey(encryptedResponse, API_KEY);
            final var jsonNode = objectMapper.readValue(response, JsonNode.class);
            final var transactionId = Optional.ofNullable(jsonNode.get("transaction_id"))
                    .map(JsonNode::asText)
                    .orElse("");
            final var wingAccount = Optional.ofNullable(jsonNode.get("wing_account"))
                    .map(JsonNode::asText)
                    .orElse("");
            final var billerName = Optional.ofNullable(jsonNode.get("customer_name"))
                    .map(JsonNode::asText)
                    .orElse("");
            final var remark = Optional.ofNullable(jsonNode.get("remark"))
                    .map(JsonNode::asText)
                    .orElse("");

            final var currency = Optional.ofNullable(jsonNode.get("currency"))
                    .map(JsonNode::asText)
                    .orElse("");

            final var transactionDate = Optional.ofNullable(jsonNode.get("tran_date"))
                    .map(JsonNode::asText)
                    .orElse("");

            final var feeAmount = Optional.ofNullable(jsonNode.get("fee_amount"))
                    .map(JsonNode::asText)
                    .orElse("");

            final var wing = repository.findByOrderId(remark)
                    .orElseThrow(() -> new ResourceNotFoundException(Wing.class, remark));
            wing.setCurrency(currency);
            wing.setTransactionDate(transactionDate);
            wing.setFeeAmount(feeAmount);
            wing.setTransId(transactionId);
            wing.setCustomerName(billerName);
            wing.setWingAccount(wingAccount);
            wing.setSuccessful(true);

            accountService.depositToUserAccount(wing.getCreatedBy().getId(), wing.getAmount(), "top up via wing", WING);

            repository.save(wing);

            return Map.of("success", true);
        } catch (final Exception e) {
            log.error("error processing callback", e);
            return Map.of("success", false);
        }
    }

    public Optional<Wing> findByTransaction(final String transactionID) {
        try {
            return repository.findByTransId(transactionID);
        } catch (Exception e) {
            log.error("findByTransaction by id error", e);
        }
        return Optional.empty();
    }
}
