package com.nisai.auction.feature.payment.domain;

public enum PaymentStatus {
    FAIL,
    SUCCESS,
    CLOSED
}
