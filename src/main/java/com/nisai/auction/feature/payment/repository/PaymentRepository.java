package com.nisai.auction.feature.payment.repository;

import com.nisai.auction.feature.payment.domain.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
}
