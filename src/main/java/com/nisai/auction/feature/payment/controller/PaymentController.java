package com.nisai.auction.feature.payment.controller;

import com.nisai.auction.feature.payment.domain.Payment;
import com.nisai.auction.feature.payment.service.PaymentService;
import com.nisai.auction.persistence.filter.FilterConfig;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/payment")
@AllArgsConstructor
public class PaymentController {

    private final FilterConfig filterConfig;
    private final PaymentService service;

    @PostMapping("/promotion/{id}")
    public Payment payForPromotion(@PathVariable Long id) {
        filterConfig.enableFilters();
        return service.payForPromotionFee(id);
    }

    @PostMapping("/product/{id}")
    public Payment payProductFee(@PathVariable Long id) {
        filterConfig.enableFilters();
        return service.payProductFee(id);
    }

    @PostMapping("/product/{id}/all")
    public List<Payment> payAllProductFee(@PathVariable Long id) {
        filterConfig.enableFilters();
        return service.payForProduct(id);
    }
}
