package com.nisai.auction.feature.payment.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Slf4j
public class WingResponseDecryptor {

    public static String decryptWithKey(String originalText, String strKey) throws Exception {

        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
            final byte[] digestOfPassword = md.digest(strKey.getBytes(StandardCharsets.UTF_8));
            final SecretKey key = new SecretKeySpec(digestOfPassword, "AES");

            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(new byte[16]));
            final byte[] plainTextBytes = Base64.decodeBase64(originalText);
            final byte[] encodeTextBytes = cipher.doFinal(plainTextBytes);

            return new String(encodeTextBytes);

        } catch (NoSuchAlgorithmException | IllegalBlockSizeException
                | InvalidKeyException | BadPaddingException | NoSuchPaddingException
                | InvalidAlgorithmParameterException e) {
            log.error("fail to decrypt response", e);
            throw e;
        }
    }
}
