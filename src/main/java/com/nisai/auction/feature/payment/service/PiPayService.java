package com.nisai.auction.feature.payment.service;

import com.nisai.auction.feature.payment.domain.PiPay;
import com.nisai.auction.feature.payment.domain.PiPayStatus;
import com.nisai.auction.feature.payment.repository.PiPayRepository;
import com.nisai.auction.user.account.service.UserAccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static com.nisai.auction.organization.account.domain.PaymentMethod.PI_PAY;

@Slf4j
@Service
@RequiredArgsConstructor
public class PiPayService {

    private static final String MID = "103905";
    private static final String DID = "15244";
    private static final String SID = "9634";
    private static final String CONFIRM_URL = "https://api.khmerauction.com/pipay/success";
    private static final String CANCEL_URL = "https://api.khmerauction.com/pipay/fail";
    private static final String URL = "https://onlinepayment-uat.pipay.com/starttransaction";

    private final PiPayRepository repository;
    private final UserAccountService accountService;

    public Map<String, String> init(final BigDecimal amount) {
        final var orderId = String.valueOf(System.currentTimeMillis());
        final var digest = DigestUtils.md5DigestAsHex((MID + orderId + amount).getBytes());
        final var pipay = new PiPay();
        pipay.setAmount(amount);
        pipay.setDigest(digest);
        pipay.setOrderId(orderId);
        pipay.setStatus(PiPayStatus._1111);
        repository.save(pipay);
        final var map = new HashMap<String, String>();
        map.put("mid", MID);
        map.put("sid", SID);
        map.put("lang", "en");
        map.put("orderid", orderId);
        map.put("currency", "USD");
        map.put("amount", amount.toString());
        map.put("trType", "2");
        map.put("confirmURL", CONFIRM_URL);
        map.put("cancelURL", CANCEL_URL);
        map.put("did", DID);
        map.put("orderDate", LocalDateTime.now().toString());
        map.put("payMethod", "wallet");
        map.put("digest", digest);
        map.put("url", URL);
        return map;
    }

    public boolean success(PiPayStatus status,
                           String transID,
                           String orderID,
                           String processorID,
                           String digest) {
        if (orderID == null) {
            return false;
        }
        final var payment = repository.findByOrderId(orderID);
        if (payment.isEmpty()) {
            return false;
        }
        final var pipay = payment.get();
        accountService.depositToUserAccount(pipay.getCreatedBy().getId(), pipay.getAmount(), "top up via pipay", PI_PAY);
        pipay.setProcessorId(processorID);
        pipay.setStatus(status);
        pipay.setDigest(digest);
        pipay.setTransId(transID);
        repository.save(pipay);
        return true;
    }

    public boolean fail(PiPayStatus status,
                        String transID,
                        String orderID,
                        String processorID,
                        String digest) {
        if (orderID == null) {
            return false;
        }
        final var piPayOptional = repository.findByOrderId(orderID);
        if (piPayOptional.isEmpty()) {
            return false;
        }
        final var pipay = piPayOptional.get();
        pipay.setProcessorId(processorID);
        pipay.setStatus(status);
        pipay.setDigest(digest);
        pipay.setTransId(transID);
        repository.save(pipay);
        return true;
    }
}
