package com.nisai.auction.feature.payment.domain;

import com.nisai.auction.persistence.domain.PaymentEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Filter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Filter(name = "userFilter", condition = "created_by_id = :id")
public class PiPay extends PaymentEntity {

    private String digest;

    private String processorId;

    @Enumerated(EnumType.STRING)
    private PiPayStatus status;
}
