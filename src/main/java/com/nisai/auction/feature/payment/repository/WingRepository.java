package com.nisai.auction.feature.payment.repository;

import com.nisai.auction.feature.payment.domain.Wing;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WingRepository extends JpaRepository<Wing, Long> {
    Optional<Wing> findByOrderId(final String orderId);

    Optional<Wing> findByTransId(final String transactionId);
}
