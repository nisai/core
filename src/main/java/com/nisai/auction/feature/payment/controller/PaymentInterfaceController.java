package com.nisai.auction.feature.payment.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nisai.auction.feature.payment.service.HttpService;
import com.nisai.auction.feature.payment.service.PiPayService;
import com.nisai.auction.feature.payment.service.WingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("/payment")
@RequiredArgsConstructor
public class PaymentInterfaceController {

    private static final String url = "https://api.khmerauction.com/wing/phally";

    private final PiPayService piPayService;
    private final WingService wingService;
    private final ObjectMapper objectMapper;

    @GetMapping("/pipay/init")
    public String getPipayConfirmPage(final Model model, @RequestParam("amount") final BigDecimal amount) {
        final var param = piPayService.init(amount);
        model.addAllAttributes(param);
        return "pipay_next";
    }

    @GetMapping("/wing/init")
    public String getWingConfirmPage(final Model model, @RequestParam("amount") final BigDecimal amount) {
        final var param = wingService.init(amount);
        model.addAllAttributes(param);
        return "wing_next";
    }

    @GetMapping("/wing/adhoc/init")
    public String init(final Model model, @RequestParam String token, @RequestParam BigDecimal amount) {
        try {
            final var response = HttpService.post(url + "?amount=" + amount, token);
            if (HttpStatus.OK.value() != response.statusCode()) {
                throw new ResponseStatusException(HttpStatus.valueOf(response.statusCode()), "invalid request");
            }
            final var body = response.body();
            final var params = objectMapper.readValue(body, new TypeReference<Map<String, Object>>() {
            });
            model.addAllAttributes(params);
            return "wing_next";
        } catch (IOException e) {
            log.error("error requesting to /phally", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
        }
    }
}
