package com.nisai.auction.feature.payment.domain;

import com.nisai.auction.persistence.domain.AuditingEntity;
import com.nisai.auction.user.account.domain.UserAccountTransaction;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Filter(name = "userFilter", condition = "created_by_id = :id")
public class Payment extends AuditingEntity {

    @OneToOne
    @JoinColumn(name = "transaction_id")
    private UserAccountTransaction transaction;

    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    private PaymentFor paymentFor;

    @Enumerated(EnumType.STRING)
    private PaymentStatus status;
}
