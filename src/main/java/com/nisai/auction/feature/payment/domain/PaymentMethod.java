package com.nisai.auction.feature.payment.domain;

public enum PaymentMethod {
    CASH,
    ABA,
    PI_PAY,
    CREDIT_CARD
}
