package com.nisai.auction.feature.payment.domain;

public enum  PaymentFor {
    PRODUCT,
    PROMOTION,
    COMMISSION
}
