package com.nisai.auction.feature.payment.controller;

import com.nisai.auction.feature.payment.domain.PiPayStatus;
import com.nisai.auction.feature.payment.service.PiPayService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pipay")
@RequiredArgsConstructor
public class PiPayController {

    private final PiPayService piPayService;

    @GetMapping("/success")
    public String paymentSuccess(String status,
                                 String transID,
                                 String orderID,
                                 String processorID,
                                 String digest) {
        piPayService.success(PiPayStatus.ofValue(status), transID, orderID, processorID, digest);
        return "";
    }


    @GetMapping("/fail")
    public String paymentFail(String status,
                              String transID,
                              String orderID,
                              String processorID,
                              String digest) {
        piPayService.fail(PiPayStatus.ofValue(status), transID, orderID, processorID, digest);
        return "";
    }
}
