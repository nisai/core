package com.nisai.auction.feature.payment.controller;

import com.nisai.auction.feature.payment.service.WingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

@RestController
@RequestMapping("/wing")
@RequiredArgsConstructor
public class WingController {

    private final WingService wingService;

    @PostMapping("/callback")
    public Map<String, Object> paymentSuccess(@RequestBody(required = false) final String body) {
        return wingService.callback(body);
    }

    @PostMapping("/return")
    public void success(@RequestParam(required = false, name = "response") final String useless,
                        HttpServletResponse response) throws IOException {
        response.sendRedirect("/wing/success");
    }

    @GetMapping("/return")
    public void fail(@RequestParam(required = false, name = "response") final String useless,
                     HttpServletResponse response) throws IOException {
        response.sendRedirect("/wing/fail");
    }

    @PostMapping("/phally")
    public Map<String, String> internalWorker(@RequestParam("amount") final BigDecimal amount) {
        return wingService.init(amount);
    }

    @RequestMapping("/fail")
    public String successPage() {
        return "";
    }

    @RequestMapping("/success")
    public String failPage() {
        return "";
    }
}
