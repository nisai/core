package com.nisai.auction.feature.payment.domain;

import lombok.Getter;

@Getter
public enum PiPayStatus {
    _1111("1111", "Created"),
    _0000("0000", "Success"),
    _0001("0001", "Failed – Payment Processor is not available"),
    _0003("0003", "Failed – User login failed"),
    _0004("0004", "Failed – Payment processor rejected the transaction"),
    _0005("0005", "Failed – Payment processor did not respond it."),
    _0006("0006", "Failed – A time out happened"),
    _0007("0007", "Failed - Payment Gateway is not available"),
    _0200("0200", "Failed - User cancel the transaction");

    private final String description;
    private final String value;

    PiPayStatus(final String value, final String description) {
        this.description = description;
        this.value = value;
    }

    public static PiPayStatus ofValue(String v) {
        for (PiPayStatus e : values()) {
            if (e.value.equals(v)) {
                return e;
            }
        }
        return null;
    }
}
