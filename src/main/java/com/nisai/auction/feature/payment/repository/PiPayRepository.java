package com.nisai.auction.feature.payment.repository;

import com.nisai.auction.feature.payment.domain.PiPay;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PiPayRepository extends JpaRepository<PiPay, Long> {
    Optional<PiPay> findByOrderId(final String orderId);
}
