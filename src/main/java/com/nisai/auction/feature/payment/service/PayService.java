package com.nisai.auction.feature.payment.service;

import com.nisai.auction.feature.payment.domain.Payment;
import com.nisai.auction.feature.payment.domain.PaymentFor;
import com.nisai.auction.feature.payment.domain.PaymentStatus;
import com.nisai.auction.feature.payment.repository.PaymentRepository;
import com.nisai.auction.user.account.domain.UserAccount;
import com.nisai.auction.user.account.service.UserAccountService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@Transactional
@AllArgsConstructor
public class PayService {

    private final PaymentRepository repository;
    private final UserAccountService accountService;

    public Payment pay(UserAccount userAccount, BigDecimal amount, PaymentFor paymentFor, String paymentDetail) {
        var userAccountTx = accountService.payToIncomeGL(userAccount, amount, paymentDetail);
        var payment = new Payment()
                .setPaymentFor(paymentFor)
                .setAmount(amount)
                .setStatus(PaymentStatus.SUCCESS)
                .setTransaction(userAccountTx);
        return repository.save(payment);
    }
}
