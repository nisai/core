package com.nisai.auction.feature.payment.service;

import com.nisai.auction.feature.payment.domain.Payment;
import com.nisai.auction.feature.payment.domain.PaymentFor;
import com.nisai.auction.feature.payment.domain.PaymentStatus;
import com.nisai.auction.feature.payment.repository.PaymentRepository;
import com.nisai.auction.feature.product.domain.ProductLabel;
import com.nisai.auction.feature.product.domain.ProductStatus;
import com.nisai.auction.feature.product.repository.ProductRepository;
import com.nisai.auction.feature.product.service.ProductFeeService;
import com.nisai.auction.feature.promotion.domain.ProductPromotion;
import com.nisai.auction.feature.promotion.domain.PromotionStatus;
import com.nisai.auction.feature.promotion.domain.PromotionType;
import com.nisai.auction.feature.promotion.repository.ProductPromotionRepository;
import com.nisai.auction.persistence.exception.ResourceNotFoundException;
import com.nisai.auction.user.account.domain.UserAccount;
import com.nisai.auction.user.account.service.UserAccountService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class PaymentService {

    private final ProductFeeService feeService;
    private final PayService payService;
    private final UserAccountService accountService;
    private final ProductRepository productRepository;
    private final ProductPromotionRepository promotionRepository;

    public List<Payment> payForProduct(final Long id) {
        final var promotions = promotionRepository.findAllByProductIdAndStatus(id, PromotionStatus.ORDERED);
        final var promotionPayments = promotions.stream()
                .map(this::payForPromotionFee)
                .collect(Collectors.toList());
        final var productPayment = payProductFee(id);
        promotionPayments.add(productPayment);
        return promotionPayments;
    }

    public Payment payForPromotionFee(Long id) {
        var productPromotion = promotionRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException(ProductPromotion.class, id));
        return payForPromotionFee(productPromotion);
    }

    public Payment payForPromotionFee(ProductPromotion productPromotion) {
        var amount = productPromotion.getPromotion().getFee();
        var account = accountService.getByUser(productPromotion.getCreatedBy());
        account.validateAmount(amount);
        productPromotion.setStatus(PromotionStatus.ON_GOING);
        promotionRepository.save(productPromotion);

        // change product label
        var product = productPromotion.getProduct();
        var promotionType = productPromotion.getPromotion().getType();
        var label = promotionType.equals(PromotionType.URGENT) ? ProductLabel.URGENT : ProductLabel.HIGH_LIGHTED;
        product.setLabel(label);
        productRepository.save(product);

        return payService.pay(account, amount, PaymentFor.PROMOTION, "payment for promotion with id: " + productPromotion.getId());
    }

    public Payment payProductFee(Long id) {
        var product = productRepository.findById(id).orElseThrow();
        var account = accountService.getByUser(product.getCreatedBy());
        var fee = feeService.getProductFee(product);
        account.validateAmount(fee);
        product.setStatus(ProductStatus.BID_IN_PROGRESS);
        product.setBidStartAt(LocalDateTime.now());
        productRepository.save(product);
        productRepository.flush();
        return payService.pay(account, fee, PaymentFor.PRODUCT, "pay for posting fee");
    }
}
