package com.nisai.auction.feature.lose.domain;

import com.nisai.auction.feature.product.domain.BidPrice;
import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.graphql.annotation.GQLIgnoreGenerate;
import com.nisai.auction.persistence.domain.VersionEntity;
import com.nisai.auction.user.authentication.domain.AppUser;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@GQLIgnoreGenerate
@Filter(name = "userFilter", condition = "user_id = :id")
@Filter(name = "readFilter", condition = "user_id = :id")
public class LoseProduct extends VersionEntity {
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private BidPrice bidPrice;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private AppUser user;
}
