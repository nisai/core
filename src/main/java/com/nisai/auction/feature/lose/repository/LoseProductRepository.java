package com.nisai.auction.feature.lose.repository;

import com.nisai.auction.feature.lose.domain.LoseProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoseProductRepository extends JpaRepository<LoseProduct, Long> {
}
