package com.nisai.auction.search.controller;

import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.search.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/search")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @GetMapping
    public List<Product> search(@RequestParam("keyword") final String keyword, Pageable pageable) {
        return searchService.manualSearch(keyword, pageable).getContent();
    }
}
