package com.nisai.auction.search.service;

import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.feature.product.repository.ProductRepository;
import lombok.extern.java.Log;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Log
@Service
public class SearchService {

    @Autowired
    private EntityManager entityManager;
    @Autowired
    private ProductRepository repository;

    public Page<Product> manualSearch(final String keyWord, Pageable pageable) {
        enableSearchFilter();
        final var keyword = "%" + keyWord + "%";
        return repository.findAllByNameLikeOrDescriptionLike(keyword, keyword, pageable);
    }

//    public List<Product> searchProduct(final String keyword, final int limit, final int offset) {
//        final var fullTextEntityManager = fullTextEntityManager();
//        final var queryBuilder = fullTextEntityManager.getSearchFactory()
//                .buildQueryBuilder()
//                .forEntity(Product.class)
//                .get();
//        final var query = queryBuilder
//                .keyword()
//                .fuzzy()
//                .withEditDistanceUpTo(1)
//                .onField("name")
//                .boostedTo(10f)
//                .andField("description")
//                .matching(keyword)
//                .createQuery();
//        final var jpaQuery = fullTextEntityManager.createFullTextQuery(query, Product.class);
//        jpaQuery.setFirstResult(offset); //start from the 15th element
//        jpaQuery.setMaxResults(limit);
//        final var resultList = (List<Product>) jpaQuery.getResultList();
//        return resultList.stream()
//                .filter(product -> ProductStatus.BID_IN_PROGRESS.equals(product.getStatus()))
//                .collect(Collectors.toList());
//    }
//
//    private FullTextEntityManager fullTextEntityManager() {
//        final var fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
//        try {
//            fullTextEntityManager.createIndexer().startAndWait();
//        } catch (InterruptedException e) {
//            log.warning(e.getMessage());
//        }
//        return fullTextEntityManager;
//    }

    private void enableSearchFilter() {
        final var session = entityManager.unwrap(Session.class);
        session.enableFilter("searchFilter");
    }
}
