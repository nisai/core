package com.nisai.auction.exception;

import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.List;
import java.util.Map;

public class GQLException extends RuntimeException implements GraphQLError {

    private GQLExceptionCode code;

    public GQLException(String message) {
        super(message);
    }

    public GQLException(String message, GQLExceptionCode code) {
        super(message);
        this.code = code;
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorType getErrorType() {
        return null;
    }

    @Override
    public Map<String, Object> getExtensions() {
        return Map.of("code", code == null ? GQLExceptionCode.FORBIDDEN : code);
    }
}
