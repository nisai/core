package com.nisai.auction.exception;

public enum GQLExceptionCode {
    INFO,
    WARNING,
    BAD_REQUEST,
    FORBIDDEN,
    UNAUTHORIZED,
    UNAUTHENTICATED,
    VALIDATION_ERROR,
    FIELD_REQUIRED
}
