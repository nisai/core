package com.nisai.auction.user.account.listener;

import com.nisai.auction.appconfiguration.utils.AutowiringHelper;
import com.nisai.auction.user.account.domain.UserRate;
import com.nisai.auction.user.account.service.UserRateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.persistence.PostPersist;

@Slf4j
public class RateListener {

    @PostPersist
    private void postPersist(final UserRate userRate) {
        TransactionSynchronizationManager.registerSynchronization(new RateTransactionSynchronizationAdapter(userRate));
    }

    @RequiredArgsConstructor
    private static class RateTransactionSynchronizationAdapter extends TransactionSynchronizationAdapter {

        private final UserRate userRate;

        @Autowired
        private UserRateService service;

        @Override
        public void afterCommit() {
            AutowiringHelper.autowire(this, service);
            service.updateUserRate(userRate.getUser().getId(), userRate.getRate());
        }
    }
}
