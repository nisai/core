package com.nisai.auction.user.account.exception;

public class InvalidPaymentCodeException extends RuntimeException{
    public InvalidPaymentCodeException() {
        super("incorrect payment code");
    }
}
