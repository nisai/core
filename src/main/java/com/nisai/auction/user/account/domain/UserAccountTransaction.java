package com.nisai.auction.user.account.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.introproventures.graphql.jpa.query.annotation.GraphQLIgnore;
import com.nisai.auction.graphql.annotation.GQLIgnoreGenerate;
import com.nisai.auction.organization.account.domain.PartyAccountType;
import com.nisai.auction.organization.account.domain.TransactionType;
import com.nisai.auction.persistence.domain.AuditingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Filter(name = "readFilter", condition = "user_account_id in (select ua.id from user_account ua where ua.user_id = :id)")
public class UserAccountTransaction extends AuditingEntity {

    @ManyToOne
    @JoinColumn(name = "user_account_id")
    @JsonIgnore
    @GraphQLIgnore
    private UserAccount userAccount;

    private BigDecimal amount = BigDecimal.ZERO;

    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @Enumerated(EnumType.STRING)
    private PartyAccountType partyAccountType;

    private Long partyAccountId;

    private String partyAccountDetail;

    private String transactionDetail;
}
