package com.nisai.auction.user.account.repository;

import com.nisai.auction.user.account.domain.UserRate;
import com.nisai.auction.user.authentication.domain.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRateRepository extends JpaRepository<UserRate, Long> {

    @Modifying
    @Query(value = "update `user` set rate = (select (sum(rate)+:newRate)/(count(rate)+1) from user_rate where user_id = :userId) where id = :userId", nativeQuery = true)
    int updateUserRate(@Param("userId") Long userId, @Param("newRate") int newRate);

    Optional<UserRate> findByUser(final AppUser user);
}
