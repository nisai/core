package com.nisai.auction.user.account.repository;

import com.nisai.auction.user.account.domain.UserAccount;
import com.nisai.auction.user.authentication.domain.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {

    Optional<UserAccount> findByUser(final AppUser user);

    Optional<UserAccount> findByUserId(final Long id);

    Optional<UserAccount> findByUserUserName(final String name);

}
