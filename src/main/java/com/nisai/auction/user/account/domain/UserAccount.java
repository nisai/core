package com.nisai.auction.user.account.domain;

import com.nisai.auction.graphql.annotation.GQLIgnoreGenerate;
import com.nisai.auction.organization.account.domain.TransactionType;
import com.nisai.auction.organization.account.exception.InsufficientBalanceException;
import com.nisai.auction.persistence.domain.VersionEntity;
import com.nisai.auction.user.account.listener.UserAccountListener;
import com.nisai.auction.user.authentication.domain.AppUser;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Filter;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Filter(name = "userFilter", condition = "user_id = :id")
@Filter(name = "readFilter", condition = "user_id = :id")
@EntityListeners(UserAccountListener.class)
@GQLIgnoreGenerate
public class UserAccount extends VersionEntity {

    @OneToOne
    @JoinColumn(columnDefinition = "user_id")
    private AppUser user;

    private BigDecimal amount = BigDecimal.ZERO;

    // amount of listing new product within a moth
    private Long monthlyList;

    // for bid success
    private Long monthlyDeal;

    // bid success deal price amount
    private BigDecimal monthlyDealAmount;

    public void increaseList() {
        if (monthlyList == null) {
            monthlyList = 1L;
        } else {
            monthlyList += 1;
        }
    }

    public UserAccountTransaction debit(BigDecimal amount) {
        this.amount = this.amount.subtract(amount);
        return new UserAccountTransaction()
                .setAmount(amount)
                .setTransactionType(TransactionType.DEBIT)
                .setUserAccount(this);
    }

    public UserAccountTransaction credit(BigDecimal amount) {
        this.amount = this.amount.add(amount);
        return new UserAccountTransaction()
                .setAmount(amount)
                .setTransactionType(TransactionType.CREDIT)
                .setUserAccount(this);
    }

    public void validateAmount(BigDecimal amount) {
        if (this.amount.compareTo(amount) < 0) {
            throw new InsufficientBalanceException();
        }
    }
}
