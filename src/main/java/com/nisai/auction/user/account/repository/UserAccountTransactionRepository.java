package com.nisai.auction.user.account.repository;

import com.nisai.auction.user.account.domain.UserAccountTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAccountTransactionRepository extends JpaRepository<UserAccountTransaction, Long> {
}
