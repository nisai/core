package com.nisai.auction.user.account.controller;

import com.nisai.auction.organization.account.domain.PaymentMethod;
import com.nisai.auction.user.account.domain.UserAccount;
import com.nisai.auction.user.account.service.UserAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserAccountController {

    private final UserAccountService service;

    @PutMapping("/{id}/account/deposit")
    @PreAuthorize("hasAuthority('ADMIN')")
    public UserAccount depositByCash(@PathVariable Long id, @RequestParam BigDecimal amount, @RequestParam String detail) {
        return service.depositToUserAccount(id, amount, detail, PaymentMethod.CASH);
    }

    @PutMapping("/{id}/account/withdraw")
    @PreAuthorize("hasAuthority('ADMIN')")
    public UserAccount withdrawByCash(@PathVariable Long id, @RequestParam BigDecimal amount, @RequestParam String detail) {
        return service.withdrawFromUserAccount(id, amount, detail, PaymentMethod.CASH);
    }
}
