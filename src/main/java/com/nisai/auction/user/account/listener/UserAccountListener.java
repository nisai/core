package com.nisai.auction.user.account.listener;

import com.nisai.auction.appconfiguration.utils.AutowiringHelper;
import com.nisai.auction.user.account.domain.UserAccount;
import com.nisai.auction.basic.notification.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.persistence.PostUpdate;
import java.math.BigDecimal;

public class UserAccountListener {

    @PostUpdate
    private void postUpdate(final UserAccount userAccount) {
        TransactionSynchronizationManager.registerSynchronization(new TxSynchronizationAdapter(userAccount));
    }

    @RequiredArgsConstructor
    private static class TxSynchronizationAdapter extends TransactionSynchronizationAdapter {

        private final UserAccount userAccount;

        @Autowired
        private NotificationService service;

        @Override
        public void afterCommit() {
            AutowiringHelper.autowire(this, service);
            if (BigDecimal.ZERO.compareTo(userAccount.getAmount()) > 0) {
                service.insufficientBalance(userAccount.getUser());
            }
        }
    }
}
