package com.nisai.auction.user.account.service;

import com.nisai.auction.user.account.repository.UserRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserRateService {

    @Autowired
    private UserRateRepository repository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateUserRate(Long userId, int newRate) {
        repository.updateUserRate(userId, newRate);
    }
}
