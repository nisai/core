package com.nisai.auction.user.account.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Getter
@Setter
@Embeddable
@Accessors(chain = true)
public class SaleSummary {

    // amount of listing new product within a moth
    private Long monthlyList;

    // for bid success
    private Long monthlyDeal;

    // bid success deal price amount
    private BigDecimal monthlyDealAmount;

    public void increaseList() {
        if (monthlyList == null) {
            monthlyList = 1L;
        } else {
            monthlyList += 1;
        }
    }
}
