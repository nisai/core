package com.nisai.auction.user.account.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.introproventures.graphql.jpa.query.annotation.GraphQLIgnore;
import com.nisai.auction.persistence.domain.AuditingEntity;
import com.nisai.auction.user.account.listener.RateListener;
import com.nisai.auction.user.authentication.domain.AppUser;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Filter;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@EntityListeners(RateListener.class)
public class UserRate extends AuditingEntity {

    @NotNull
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "user_id")
    private AppUser user;

    @NotNull
    private int rate;

    private String comment;
}
