package com.nisai.auction.user.account.service;

import com.nisai.auction.basic.notification.service.NotificationService;
import com.nisai.auction.feature.product.domain.Product;
import com.nisai.auction.feature.product.domain.ProductStatus;
import com.nisai.auction.organization.account.domain.PartyAccountType;
import com.nisai.auction.organization.account.domain.PaymentMethod;
import com.nisai.auction.organization.account.repository.GLAccountRepository;
import com.nisai.auction.organization.account.repository.GLAccountTransactionRepository;
import com.nisai.auction.organization.account.service.GLAccountService;
import com.nisai.auction.persistence.exception.ResourceNotFoundException;
import com.nisai.auction.user.account.domain.UserAccount;
import com.nisai.auction.user.account.domain.UserAccountTransaction;
import com.nisai.auction.user.account.repository.UserAccountRepository;
import com.nisai.auction.user.account.repository.UserAccountTransactionRepository;
import com.nisai.auction.user.authentication.domain.AppUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class UserAccountService {

    private final UserAccountRepository repository;
    private final GLAccountService glAccountService;
    private final NotificationService notificationService;
    private final GLAccountRepository glAccountRepository;
    private final UserAccountTransactionRepository transactionRepository;
    private final GLAccountTransactionRepository glAccountTransactionRepository;

    public UserAccount depositToUserAccount(Long id, BigDecimal amount, String details, PaymentMethod method) {
        var account = repository.findByUserId(id).orElseThrow(() -> new ResourceNotFoundException(UserAccount.class, " user id:" + id));

        // credit to user account
        var accountTx = account.credit(amount)
                .setPartyAccountType(PartyAccountType.DEPOSIT)
                .setTransactionDetail(details);

        // credit to User GL
        var glAccount = glAccountService.getGlAccount(method);
        var glTx = glAccount.credit(amount)
                .setPartyAccountId(account.getId())
                .setPartyAccountType(PartyAccountType.DEPOSIT)
                .setTransactionDetail(details);

        // todo Add GL for each payment Gateway

        // credit to Main GL
        var mainGl = glAccountService.getMainGLAccount();
        var mainGLTx = mainGl.credit(amount)
                .setPartyAccountType(PartyAccountType.DEPOSIT)
                .setPartyAccountId(account.getId())
                .setTransactionDetail(details);

        // send notification
        notificationService.topUp(account, amount);

        repository.save(account);
        transactionRepository.save(accountTx);
        glAccountRepository.save(glAccount);
        glAccountTransactionRepository.save(glTx);
        glAccountRepository.save(mainGl);
        glAccountTransactionRepository.save(mainGLTx);
        return account;
    }

    public UserAccount withdrawFromUserAccount(Long id, BigDecimal amount, String details, PaymentMethod method) {
        var account = repository.findByUserId(id).orElseThrow(() -> new ResourceNotFoundException(UserAccount.class, " user id:" + id));

        // check account balance
        if (account.getAmount().compareTo(amount) < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "insufficient balance");
        }
        account.setAmount(account.getAmount().subtract(amount));

        // debit from user account
        var accountTx = account.debit(amount)
                .setPartyAccountType(PartyAccountType.WITHDRAW)
                .setTransactionDetail(details);

        // debit from User GL
        var glAccount = glAccountService.getGlAccount(method);
        var glTx = glAccount.debit(amount)
                .setPartyAccountId(account.getId())
                .setPartyAccountType(PartyAccountType.WITHDRAW)
                .setTransactionDetail(details);

        // debit from Main GL
        var mainGl = glAccountService.getMainGLAccount();
        var mainGLTx = mainGl.debit(amount)
                .setPartyAccountType(PartyAccountType.WITHDRAW)
                .setPartyAccountId(account.getId())
                .setTransactionDetail(details);

        repository.save(account);
        transactionRepository.save(accountTx);
        glAccountRepository.save(glAccount);
        glAccountTransactionRepository.save(glTx);
        glAccountRepository.save(mainGl);
        glAccountTransactionRepository.save(mainGLTx);
        return account;
    }

    public UserAccountTransaction payToIncomeGL(UserAccount account, BigDecimal amount, String detail) {

        var incomeGl = glAccountService.getIncomeGLAccount();

        // user account debit tx
        var accountTx = account.debit(amount)
                .setTransactionDetail(detail)
                .setPartyAccountId(incomeGl.getId())
                .setPartyAccountType(PartyAccountType.GL_ACCOUNT);

        // credit into income account
        var incomeGlTx = incomeGl.credit(amount)
                .setTransactionDetail(detail)
                .setPartyAccountType(PartyAccountType.USER_ACCOUNT)
                .setPartyAccountId(account.getId());

        // send notification
        notificationService.feeCharge(account, amount);

        repository.save(account);
        transactionRepository.save(accountTx);
        glAccountRepository.save(incomeGl);
        glAccountTransactionRepository.save(incomeGlTx);
        return accountTx;
    }

    @Async
    public void updateUserSummary(Product product) {
        var account = repository
                .findByUser(product.getCreatedBy())
                .orElse(null);
        if (account == null) {
            account = new UserAccount()
                    .setUser(product.getCreatedBy());
        }
        if (ProductStatus.BID_IN_PROGRESS.equals(product.getStatus())
                && product.getStatus() != product.getPreviousStatus()) {
            account.increaseList();
            repository.saveAndFlush(account);
        }
    }

    public UserAccount getByUserName(final String name) {
        return repository.findByUserUserName(name).orElseThrow();
    }

    public UserAccount getByUser(final AppUser user) {
        return repository.findByUser(user).orElseThrow(() -> new ResourceNotFoundException(AppUser.class, "with user id " + user.getId()));
    }
}
