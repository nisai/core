package com.nisai.auction.user.authentication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AppUserService service;

    public Boolean resetPassword(final String name, final String newPassword) {
       try{
           final var appUser = isPhoneNumber(name)
                   ? service.getUserPhoneNumber(name)
                   : isEmail(name)
                   ? service.getUserByEmail(name)
                   : service.getUserByUserName(name);

           if (appUser == null) {
               return false;
           }

          return service.resetPassword(appUser, newPassword);

       } catch (Exception e) {
           return false;
       }
    }

    @Override
    public UserAuthentication loadUserByUsername(final String name) throws UsernameNotFoundException {
        final var appUser = isPhoneNumber(name)
                ? service.getUserPhoneNumber(name)
                : isEmail(name)
                ? service.getUserByEmail(name)
                : service.getUserByUserName(name);
        final var authority = new SimpleGrantedAuthority(appUser.getRole().name());
        return new UserAuthentication(appUser.getUserName(), appUser.getPassword(), Collections.singletonList(authority), appUser);
    }

    private boolean isPhoneNumber(String name) {
        return name.matches("^[0-9]{9,10}$");
    }

    private boolean isEmail(String name) {
        return name.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
    }
}
