package com.nisai.auction.user.authentication.controller;

import com.nisai.auction.appconfiguration.utils.ApplicationSecurityContext;
import com.nisai.auction.exception.PasswordInvalidException;
import com.nisai.auction.feature.address.domain.Province;
import com.nisai.auction.feature.address.repository.ProvinceRepository;
import com.nisai.auction.persistence.exception.ResourceNotFoundException;
import com.nisai.auction.persistence.service.EntityDataMapper;
import com.nisai.auction.user.account.domain.UserAccount;
import com.nisai.auction.user.account.repository.UserAccountRepository;
import com.nisai.auction.user.authentication.data.AppUserData;
import com.nisai.auction.user.authentication.data.AppUserMapper;
import com.nisai.auction.user.authentication.domain.AppUser;
import com.nisai.auction.user.authentication.repository.AppUserRepository;
import com.nisai.auction.user.authentication.service.AppUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class AppUserController {

    private final PasswordEncoder encoder;
    private final AppUserRepository repository;
    private final AppUserService service;
    private final EntityDataMapper entityDataMapper;
    private final ApplicationSecurityContext context;
    private final UserAccountRepository accountRepository;
    private final AppUserMapper userMapper;
    private final ProvinceRepository provinceRepository;

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public List<AppUser> getAllUser() {
        return repository.findAll();
    }

    @GetMapping("/current")
    public AppUser getCurrentUser() {
        var user = context.authenticatedUser();
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "unauthorized request");
        }
        return user;
    }

    @GetMapping("/current/account")
    public UserAccount getCurrentUserAccount() {
        var user = context.authenticatedUser();
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "unauthorized request");
        }
        return accountRepository.findByUser(user).orElse(null);
    }

    @GetMapping("/{id}/account")
    @PreAuthorize("hasAuthority('ADMIN')")
    public UserAccount getUserAccountById(@PathVariable Long id) {
        return accountRepository.findByUserId(id).orElseThrow();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public AppUser getUserById(@NotNull @PathVariable("id") final Long id) {
        return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(AppUser.class, id));
    }

    @PutMapping("/changePassword")
    public AppUser changePassword(@RequestParam("old") final String oldPassword,
                                  @RequestParam("new") final String newPassword) {
        final var user = context.authenticatedUser();

        if (user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "user cannot be null");
        }

        if (encoder.matches(oldPassword, user.getPassword())) {
            final var appUser = repository.findById(user.getId()).orElseThrow();
            appUser.setPassword(encoder.encode(newPassword));
            return repository.save(appUser);
        }
        throw new PasswordInvalidException();
    }

    @PutMapping("/current/update")
    public AppUser updateCurrent(@RequestBody final AppUser sourceUser) {
        var contextUser = context.authenticatedUser();
        if (contextUser == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "unauthorized request");
        }
        if (sourceUser.getPassword() != null) {
            sourceUser.setPassword(encoder.encode(sourceUser.getPassword()));
        }
        try {
            final var user = entityDataMapper.mapObject(sourceUser, contextUser, AppUser.class);
            return repository.save(user);
        } catch (Exception exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage(), exception);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public AppUser updateUser(@PathVariable final Long id, @RequestBody final AppUser sourceUser) {
        final var targetUser = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(AppUser.class, id));
        if (sourceUser.getPassword() == null) {
            sourceUser.setPassword(targetUser.getPassword());
        } else {
            sourceUser.setPassword(encoder.encode(sourceUser.getPassword()));
        }
        try {
            final var user = entityDataMapper.mapObject(sourceUser, targetUser, AppUser.class);
            return repository.save(user);
        } catch (Exception exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage(), exception);
        }
    }

    @GetMapping("/find-email")
    public AppUser getUserByEmail(@NotNull @RequestParam("email") final String email) {
        return repository.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException(AppUser.class, email));
    }

    @PostMapping
    public ResponseEntity<?> createUser(@Valid @RequestBody final AppUserData request) {
        try {
            var response = new HashMap<String, Object>();
            var error = false;
            var message = "";
            try {
                final var appUser = service.getUserPhoneNumber(request.getMobile());
                if (appUser != null) {
                    error = true;
                    message = "your phone number already exist";
                }
            } catch (Exception ignored) {

            }

            try {
                final var appUser = service.getUserByEmail(request.getEmail());
                if (appUser != null) {
                    error = true;
                    message = "your email already exist";
                }
            } catch (Exception ignored) {

            }

            try {
                final var appUser = service.getUserByUserName(request.getUserName());
                if (appUser != null) {
                    error = true;
                    message = "your username already exist";
                }
            } catch (Exception ignored) {

            }

            if (error) {
                response.put("error", true);
                response.put("message", message);
                response.put("data", null);
                return ResponseEntity.ok(response);
            }
            var appUser = userMapper.map(request);

            if (request.getProvinceId() != null) {
                var province = provinceRepository.findById(request.getProvinceId())
                        .orElseThrow(() -> new ResourceNotFoundException(Province.class, request.getProvinceId()));
                appUser.setProvince(province);
            }

            var user = service.createUser(appUser);
            response.put("error", false);
            response.put("message", null);
            response.put("data", user);
            return ResponseEntity.ok(response);
        } catch (final Exception ignored) {

        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public void deleteUser(@NotNull @PathVariable("id") final Long id) {
        repository.deleteById(id);
    }
}
