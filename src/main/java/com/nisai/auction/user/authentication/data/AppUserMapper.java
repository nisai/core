package com.nisai.auction.user.authentication.data;

import com.nisai.auction.user.authentication.domain.AppUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.function.Function;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface AppUserMapper extends Function<AppUser, AppUserData> {

    @Mapping(source = "provinceId", target = "province.id")
    @Mapping(source = "provinceName", target = "province.name")
    AppUser map(AppUserData appUserData);

    AppUserData apply(AppUser appUser);
}
