package com.nisai.auction.user.authentication.repository;

import com.nisai.auction.user.authentication.domain.AppUser;
import com.nisai.auction.user.authentication.domain.FireBaseToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FireBaseTokenRepository extends JpaRepository<FireBaseToken, Long> {

    List<FireBaseToken> findAllByCreatedBy(AppUser createdBy);
}
