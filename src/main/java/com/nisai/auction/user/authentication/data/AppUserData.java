package com.nisai.auction.user.authentication.data;

import com.nisai.auction.user.authentication.domain.UserRole;
import com.nisai.auction.user.authentication.domain.UserStatus;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Getter
@Setter
public class AppUserData {
    private Long id;
    private LocalDateTime createdAt;
    @NotBlank
    private String userName;
    private String firstName;
    private String lastName;
    private String displayName;
    @Email
    private String email;
    private String mobile;
    private String profileImage;
    private String photoUrl;
    @NotBlank
    private String password;
    private UserRole role;
    private UserStatus status;
    private Long provinceId;
    private String provinceName;
    private int rate;
}
