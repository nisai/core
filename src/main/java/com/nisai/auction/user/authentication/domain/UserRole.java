package com.nisai.auction.user.authentication.domain;

public enum UserRole {
    ADMIN,
    USER
}
