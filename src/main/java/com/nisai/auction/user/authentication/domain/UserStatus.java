package com.nisai.auction.user.authentication.domain;

public enum UserStatus {
    ACTIVE,
    INACTIVE,
    VERIFIED,
    BLOCKED,
    DELETED
}
