package com.nisai.auction.user.authentication.domain;

import com.nisai.auction.persistence.domain.AuditingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class FireBaseToken extends AuditingEntity {

    private String token;
}
