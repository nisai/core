package com.nisai.auction.user.authentication.controller;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.nisai.auction.appconfiguration.utils.JwtUtil;
import com.nisai.auction.appconfiguration.utils.TokenConfig;
import com.nisai.auction.persistence.exception.ResourceNotFoundException;
import com.nisai.auction.user.authentication.data.AuthenticationData;
import com.nisai.auction.user.authentication.data.AuthenticationRequest;
import com.nisai.auction.user.authentication.data.TokenData;
import com.nisai.auction.user.authentication.domain.AppUser;
import com.nisai.auction.user.authentication.domain.UserRole;
import com.nisai.auction.user.authentication.service.AppUserService;
import com.nisai.auction.user.authentication.service.UserDetailsServiceImpl;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

@Slf4j
@RestController
@RequestMapping("auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final UserDetailsServiceImpl userDetailsService;
    private final AppUserService appUserService;
    private final TokenConfig tokenConfig;
    private final JwtUtil jwtUtil;

    @PostMapping("token")
    public AuthenticationData createAuthenticationToken(@RequestBody @Valid AuthenticationRequest request) {
        var authenticationToken = new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
        authenticationManager.authenticate(authenticationToken);
        var userDetails = userDetailsService.loadUserByUsername(request.getUsername());
        return jwtUtil.generateToken(userDetails);
    }

    @PostMapping("reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody @Valid AuthenticationRequest request) {
        final var response = new HashMap<>();
        try {
            var success = userDetailsService.resetPassword(request.getUsername(), request.getPassword());
            if (success) {
                return new ResponseEntity<>(HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Cannot reset password");
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/token/firebase")
    public AuthenticationData createTokenWithFirebaseToken(@RequestBody @Valid TokenData tokenData) {
        try {
            var decodedToken = FirebaseAuth.getInstance().verifyIdToken(tokenData.getToken());
            try {
                var userdetails = userDetailsService.loadUserByUsername(decodedToken.getUid());
                return jwtUtil.generateToken(userdetails);
            } catch (ResourceNotFoundException e) {
                var appUser = new AppUser()
                        .setFirebaseUser(true)
                        .setEmail(decodedToken.getEmail())
                        .setUserName(decodedToken.getUid())
                        .setPassword("123!@#")
                        .setRole(UserRole.USER)
                        .setPhotoUrl(decodedToken.getPicture());
                appUserService.createUser(appUser);
                var userdetails = userDetailsService.loadUserByUsername(decodedToken.getUid());
                return jwtUtil.generateToken(userdetails);
            }
        } catch (FirebaseAuthException e) {
            log.error("fail to validate firebase token", e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "token invalid");
        }
    }

    @PostMapping("refresh-token")
    public ResponseEntity<?> refreshToken(@RequestBody @Valid TokenData tokenData) {

        // get claims
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(tokenConfig.getSecret())
                    .parseClaimsJws(tokenData.getToken())
                    .getBody();
        } catch (ExpiredJwtException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Refresh token is expired");
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid refresh token");
        }

        // validate token type
        if (!claims.containsKey("refreshToken")) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Provided token is not refresh token");
        }

        var expectedMap = getMapFromIoJsonWebTokenClaims(claims);
        var subject = expectedMap.get("sub").toString();

        // generate refresh token
        var refreshToken = jwtUtil.generateRefreshToken(expectedMap, subject);

        // generate token
        expectedMap.remove("refreshToken");
        var authToken = jwtUtil.generateToken(expectedMap, subject);

        return ResponseEntity.ok(new AuthenticationData(authToken, refreshToken));
    }

    private Map<String, Object> getMapFromIoJsonWebTokenClaims(Claims claims) {
        return new HashMap<>(claims);
    }

}
