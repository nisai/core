package com.nisai.auction.user.authentication.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.introproventures.graphql.jpa.query.annotation.GraphQLIgnore;
import com.nisai.auction.feature.address.domain.Province;
import com.nisai.auction.graphql.annotation.GQLIgnoreGenerate;
import com.nisai.auction.persistence.domain.VersionEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.*;
import org.springframework.util.StringUtils;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "user")
@Accessors(chain = true)
@Where(clause = "status <> 'DELETED'")
@FilterDef(name = "userFilter", defaultCondition = "created_by_id = :id", parameters = @ParamDef(name = "id", type = "long"))
@SQLDelete(sql = "update user set status = 'DELETED' where id = ? and version = ?")
@Filter(name = "userFilter", condition = "id = :id")
@JsonIgnoreProperties(value = {"role", "status"}, allowGetters = true)
public class AppUser extends VersionEntity {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;

    @NotBlank
    @Column(name = "user_name", unique = true, nullable = false, updatable = false)
    private String userName;

    private String firstName;

    private String lastName;

    private String displayName;

    @Email
    @Column(unique = true)
    private String email;

    @Column(unique = true)
    private String mobile;

    private String profileImage;

    private String photoUrl;

    @NotBlank
    @GraphQLIgnore
    private String password;

    @Enumerated(EnumType.STRING)
    private UserRole role;

    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @ManyToOne
    @JoinColumn(name = "province_id")
    private Province province;

    @GQLIgnoreGenerate
    @Column(columnDefinition = "smallint default 0")
    private int rate;

    private boolean firebaseUser;

    @PrePersist
    @PreUpdate
    private void prePersist() {
        if (status == null) {
            status = UserStatus.ACTIVE;
        }
        if (createdAt == null) {
            createdAt = LocalDateTime.now();
        }
        if (role == null) {
            role = UserRole.USER;
        }
        if (!StringUtils.hasText(displayName)) {
            displayName = StringUtils.hasText(email) ? email : mobile;
        }
    }

    public String getDisplayName() {
        return StringUtils.hasText(displayName) ? displayName : StringUtils.hasText(email) ? email : mobile;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public AppUser setPassword(final String password) {
        this.password = password;
        return this;
    }
}
