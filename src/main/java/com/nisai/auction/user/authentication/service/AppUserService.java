package com.nisai.auction.user.authentication.service;

import com.nisai.auction.persistence.exception.ResourceNotFoundException;
import com.nisai.auction.user.account.domain.UserAccount;
import com.nisai.auction.user.account.repository.UserAccountRepository;
import com.nisai.auction.user.authentication.domain.AppUser;
import com.nisai.auction.user.authentication.repository.AppUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class AppUserService {

    private final AppUserRepository repository;
    private final UserAccountRepository accountRepository;
    private final PasswordEncoder encoder;

    public AppUser getUserByUserName(final String name) {
        return repository.findByUserName(name).orElseThrow(() -> new ResourceNotFoundException(AppUser.class, name));
    }

    public AppUser getUserByEmail(final String email) {
        return repository.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException(AppUser.class, email));
    }

    public AppUser getUserPhoneNumber(final String phone) {
        return repository.findByMobile(phone).orElseThrow(() -> new ResourceNotFoundException(AppUser.class, phone));
    }

    public AppUser createUser(final AppUser appUser) {
        appUser.setPassword(encoder.encode(appUser.getPassword()));
        var user = repository.save(appUser);
        var account = new UserAccount().setUser(user);
        accountRepository.save(account);
        return user;
    }

    public boolean resetPassword(final AppUser appUser, final String newPassword) {
        try {
            appUser.setPassword(encoder.encode(newPassword));
            var user = repository.save(appUser);
            return true;
        } catch (Exception e) {

        }
        return false;
    }
}
