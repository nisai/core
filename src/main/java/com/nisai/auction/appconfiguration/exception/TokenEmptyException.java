package com.nisai.auction.appconfiguration.exception;

public class TokenEmptyException extends RuntimeException{
    public TokenEmptyException() {
        super("Access token is empty");
    }
}
