package com.nisai.auction.appconfiguration.exception;

public class TokenViolationException extends RuntimeException {
    public TokenViolationException() {
        super("Violation use of refresh token");
    }
}
