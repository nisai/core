package com.nisai.auction.appconfiguration;

import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.AbstractRequestLoggingFilter;
import org.thymeleaf.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 1)
@Slf4j
public class RequestAuditFilter extends AbstractRequestLoggingFilter {

    private static final AntPathMatcher MATCHER = new AntPathMatcher();

    @PostConstruct
    protected void setUp() {
        setAfterMessagePrefix("request=");
        setAfterMessageSuffix("");
        setIncludePayload(true);
        setMaxPayloadLength(1000);
        setIncludeQueryString(true);
    }

    @Override
    protected boolean shouldNotFilter(@NonNull final HttpServletRequest request) {
        return request.getMethod().equals("OPTIONS");
    }

    @Override
    protected void beforeRequest(@NonNull final HttpServletRequest request,
                                 @NonNull final String message) {
    }

    @Override
    protected void afterRequest(@NonNull final HttpServletRequest request,
                                @NonNull final String message) {
        final var isGQL = MATCHER.match("/graphql", request.getRequestURI());
        final var finalBody = isGQL ? StringUtils.unescapeJava(message).replaceAll("\n", "") : message;
        logger.info(finalBody);
    }

    @Override
    protected void doFilterInternal(@NonNull final HttpServletRequest request,
                                    @NonNull final HttpServletResponse response,
                                    @NonNull final FilterChain filterChain) throws ServletException, IOException {
        try {
            super.doFilterInternal(request, response, filterChain);
        } catch (final Exception e) {
            if (!(e instanceof ClientAbortException)) {
                throw e;
            }
        }
    }
}