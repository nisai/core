package com.nisai.auction.appconfiguration.utils;

import com.nisai.auction.user.authentication.domain.AppUser;
import com.nisai.auction.user.authentication.service.AppUserService;
import com.nisai.auction.user.authentication.service.UserAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class ApplicationSecurityContext {

    @Autowired
    private AppUserService service;

    public Object getAuthPrinciple() {
        final SecurityContext context = SecurityContextHolder.getContext();
        final Authentication auth = context == null ? null : context.getAuthentication();
        return auth == null ? null : auth.getPrincipal();
    }

    public AppUser authenticatedUser() {
        final var principal = getAuthPrinciple();
        if (principal instanceof UserAuthentication) {
            return ((UserAuthentication) principal).getAppUser();
        } else if (principal instanceof String && !((String) principal).equalsIgnoreCase("anonymousUser")) {
            return service.getUserByUserName((String) principal);
        }
        return null;
    }
}
