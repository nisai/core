package com.nisai.auction.appconfiguration.utils;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class TokenConfig {

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expirationDate:100000}")
    private long jwtExpiration;

    @Value("${jwt.refreshExpirationDateInMs:500000}")
    private long refreshExpirationDate;

}
