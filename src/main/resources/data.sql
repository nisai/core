-- default GL account for cash transaction
insert into glaccount(id, version, code, amount, name) values (1,0, '100001', 0,'CASH_GL');
insert into glaccount(id, version, code, amount, name) values (2,0, '100002', 0,'ABA_GL');
insert into glaccount(id, version, code, amount, name) values (3,0, '100003', 0,'PI_PAY_GL');
insert into glaccount(id, version, code, amount, name) values (4,0, '100004', 0,'CREDIT_CARD_GL');
insert into glaccount(id, version, code, amount, name) values (9,0, '100009', 0,'MAIN_GL');
insert into glaccount(id, version, code, amount, name) values (10,0, '100010', 0,'MAIN_INCOME_GL');

insert into system_config(code, value, description) values
('BID_14_DAY_FEE','0.2','fee for posting 14 days'),
('BID_30_DAY_FEE','0.4','fee for posting 30 days'),
('COMMISSION_FEE','2','comission fee of a deal as percent'),
('SUB_TITLE_FEE','0.1','fee for subtitle'),
('INCREMENT_AMOUNT','0.1','amount auto increment for auto bid'),
('LABEL_NEW_DURATION','6','duration that a product should marked as new (as hour)'),
('LABEL_FINISH_SOON_DURATION','6','duration that a product should marked as closing soon (as hour)'),