systemctl stop nginx
certbot renew
cd /etc/letsencrypt/live/api.khmerauction.com
openssl pkcs12 -export -out cert.p12 -in cert.pem -certfile chain.pem -inkey privkey.pem
cp cert.p12 /root/core/src/main/resources/
cd /root/core
mv src/main/resources/cert.p12 src/main/resources/keystore.p12
./autodeploy.sh
systemctl start nginx
