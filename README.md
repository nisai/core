# Khmer Auction Core Application

This is api application for web and mobile

## Application Environment

JDK 11 +

Memory 4G

MySQL8 Database

## Run On Production Server

1. SSH Into Production Server
- Host: `128.199.168.254`
- User: `root`
- Pwd: `P@ssw0rdPP`

2. Build And Deploy App
- Go to core directory: `cd core`
- Run script: `./autodeploy.sh`

3. Check Server Status
- Run script: `cat nohup.out`

## API Docs

Url: https://api.khmerauction.com/api/swagger-ui.html

## Renew SSL Certificate
```
disable port 80 
sudo certbot renew
# convert to pk12
openssl pkcs12 -export -out cert.p12 -in cert.pem -certfile chain.pem -inkey privkey.pem
# copy to Application server
cp cert.p12 /core/src/main/resources/
restart server
enable port 80
```