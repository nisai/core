git pull
kill $(cat ./bin/shutdown.pid)
./gradlew bootJar
kill $(ps -aux | grep gradle |grep -v grep | awk '{print $2}')
sleep 5s
nohup java -Xmx3092m  -Xms3092m -XX:+UseParallelGC -XX:-UseGCOverheadLimit -jar ./build/libs/auction-0.0.1.jar &
echo > nohup.out
